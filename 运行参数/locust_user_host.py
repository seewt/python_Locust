from locust import HttpUser,TaskSet,task,User,between,constant

'''
在用户类申明了一个 host属性，将自动用于在--host命令 或者webui 中的host
'''

class MyUser(HttpUser):
    # host ='http://wthrcdn.etouch.cn'

    def task_1(self):
        params = {'city':'杭州'}
        res =self.client.request(method='get', url='/weather_mini',params=params)


    tasks = [task_1]


if __name__ == '__main__':
    import os
    os.system('locust -f locust_user_host.py --headless -u 1000 -r 100 --run-time 20s  -H "http://wthrcdn.etouch.cn"')
