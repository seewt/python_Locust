# 一个事物下不同任务权重 @task(int：num)

from locust import TaskSet,HttpUser,task,user
import os

# 任务集合，task2 的执行量是task1的3倍
class task_s(TaskSet):

    # @task
    def task1(self):
        print('执行子任务1')
    # @task
    def task2(self):
        print('执行子任务2')

    # @task
    def stop(self):
        self.interrupt()

class task_s1(TaskSet):

    # @task
    def task2(self):
        print('主任务执行')

    tasks = {task_s: 3}

class task_conf(HttpUser):
    tasks = [task_s1]
    min_time=2000
    max_time=5000

if __name__ == '__main__':
    os.system('locust -f demo3.py')