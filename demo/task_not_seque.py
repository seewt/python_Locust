# 一个事物下不同任务权重 @task(int：num)

from locust import TaskSet, HttpUser, task, Locust, SequentialTaskSet
import os


''' 
继承TaskSet，不按顺序执行任务,
'''
class task_s(TaskSet):

    def task1(self):
        print('执行任务1')


    def task2(self):
        print('执行任务2')


    def task3(self):
        print('执行任务3')

    # 通过tasks 定义测试任务 并设置对应任务执行权重
    # tasks = {task1:1,task2:2,task3:2}
    # 通过tasks 定义测试任务
    tasks = [task1,task2,task3]

class task_conf(HttpUser):
    tasks = [task_s]
    min_time = 2000
    max_time = 5000


if __name__ == '__main__':
    os.system('locust -f task_not_seque.py')