# 一个事物下不同任务权重 @task(int：num)

from locust import TaskSet,HttpUser,task,Locust,constant,User,user
import os



def task1(user):
    print('执行任务1')

def task2(user):
    print('执行任务2')

class task_conf(User):
    # 设置tasks 属性 也可以定义用户任务
    tasks = {task1:6,task2:1}
    wait_time = constant(1)

if __name__ == '__main__':
    os.system('locust -f demo4.py')