# 不同事物间的权重 tasks= {tasks_1:1,tasks_2:3}
from locust import TaskSet,HttpUser,task,Locust
import os


# 任务集合
class task_s1(TaskSet):
    def on_start(self):
        print('启动任务1------------------------')
    @task
    def task1(self):
        print('执行任务1#################')

class task_s2(TaskSet):
    def on_start(self):
        print('启动任务2')

    @task
    def task2(self):
        print('执行任务2')

class task_conf(HttpUser):
    # task_s2 的执行量是 task_s1 的2倍
    tasks = {task_s1:1,task_s2:2}
    min_time=2000
    max_time=5000


if __name__ == '__main__':
    os.system('locust -f task_dict_demo.py')