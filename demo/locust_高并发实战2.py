import time

from locust import task,FastHttpUser,constant
import os,jsonpath

class task_s(FastHttpUser):

    wait_time = constant(0)
    host = 'http://wthrcdn.etouch.cn'
    @task
    def task_1(self):
        get_url = '/weather_mini?city=杭州'
        # res = self.client.request(method='get', path=get_url)
        # print(res.json())
        with self.client.request(method='get', path=get_url,catch_response=True) as response:
            city = jsonpath.jsonpath(response.json(), '$..city')
            if city !=['杭州']:
                response.failure('断言失败')
            #判断接口响应时间
            # elif response.elapsed.total_seconds() >0.5:
            #     print(response.elapsed.total_seconds())
            #     response.failure('响应时间过长')
            # else:
            #     response.success()


if __name__ == '__main__':
    os.system('start locust -f locust_高并发实战.py  --master --web-host="127.0.0.1"')
    for i in range(5):
            os.system('start locust -f locust_高并发实战2.py  --worker')





