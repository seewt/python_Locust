from locust import HttpUser, TaskSet, task, User, between
import requests


# Testlocust类继承 TaskSet 类，用于描述用户行为，用户 要做什么 行为都放在这个类里面

# 通过生成器函数读取本地文件数据，每调用一次，返回一个值

def get_cookie():
    with open('/Users/byy/iotest/info.txt', 'r') as f1:
        info = f1.readlines()   #读取所有数据
        for i in info:
            yield eval(i)    #eval 将string 类型数据转换为 dict，比如：'{'a':1,'b':2}'==》 {'a':1,'b':2}
g = get_cookie()

class Testlocust(TaskSet):

    def on_start(self):
        print('start')
        self.headers=next(g)

    @task
    # get_info 是一个用户行为 这里是调用某个接口
    def get_info(self):
        # with self.client.get('api/livestream/mypage/info',headers=self.headers) as response:
        #     pass
        #     # print(response.content
        response = self.client.get('api/livestream/mypage/info',headers=self.headers)#client.get 是请求路径
        # print(r.status_code)
        print(response.text)     #获取返回值的字符串对象
        print(type(response.text))
        print(response.content)     #字节流 类型
        print(type(response.content))
        print(response.json())    #dict 类型x
        print(type(response.json()))
        print(response)    #http response 对象类型
        print(type(response))

        # assert response.status_code ==200


# 用于设置性能测试 的一些参数
class WebsiteUser(HttpUser):

    tasks = [Testlocust]   #指向一个定义的用户行为类
    # 执行事务之间用户等待时间的下界和上限
    min_wait=1500
    max_wait=5000

if __name__ == '__main__':
    import os
    os.system('locust -f http_api_demo.py --host=http://api.look.163.com/')

