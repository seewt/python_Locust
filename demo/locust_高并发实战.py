import time

from locust import task,FastHttpUser,constant,HttpUser
import os,jsonpath

class task_s(HttpUser):
    host = 'http://wthrcdn.etouch.cn'
    @task
    def task_1(self):
        get_url = '/weather_mini?city=杭州'
        # res = self.client.request(method='get', path=get_url)
        # print(res.json())
        with self.client.request(method='get', url=get_url,catch_response=True) as response:
            city = jsonpath.jsonpath(response.json(), '$..city')
            if city !=['杭州']:
                response.failure('断言失败')


if __name__ == '__main__':
    os.system('start locust -f locust_高并发实战.py  --master')
    for i in range(5):
            os.system('start locust -f locust_高并发实战.py  --worker')





