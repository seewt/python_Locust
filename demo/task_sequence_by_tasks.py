# 一个事物下不同任务权重 @task(int：num)

from locust import TaskSet, HttpUser, task, Locust, SequentialTaskSet
import os

# 按顺序执行任务
class task_s(SequentialTaskSet):

    def task1(self):
        print('执行任务1')


    def task2(self):
        print('执行任务2')


    def task3(self):
        print('执行任务3')

    # 通过tasks 设置任务执行顺序
    tasks =[task3,task2,task1]


class task_conf(HttpUser):
    tasks = [task_s]
    min_time=2000
    max_time=5000

if __name__ == '__main__':
    os.system('locust -f task_sequence_by_tasks.py')