# 一个事物下不同任务权重 @task(int：num)

from locust import TaskSet,HttpUser,task,Locust
import os


# 任务集合，task2 的执行量是task1的3倍
class task_s(TaskSet):
    def on_start(self):
        print('启动任务')
    @task(1)
    def task1(self):
        print('执行任务1')
    @task(3)
    def task2(self):
        print('执行任务2')

class task_conf(HttpUser):
    tasks = [task_s]
    min_time=2000
    max_time=5000

if __name__ == '__main__':
    os.system('locust -f tasks_list_demo.py')