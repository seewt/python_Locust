from locust import HttpUser,TaskSet,task


# Testlocust类继承 TaskSet 类，用于描述用户行为，用户 要做什么 行为都放在这个类里面
n=0
cookies = [{'cookie': 11111},{'cookie': 22222},{'cookie': 333333},{'cookie': 44444},{'cookie':555555},{'cookie':6666666}]
class Testlocust(TaskSet):
    def on_start(self):
        global n
        self.cookie = cookies[n]
        n+=1
    @task
    # get_info 是一个用户行为 这里是调用某个接口
    def get_info(self):
        print(self.cookie)
        # r = self.client.get('api/livestream/mypage/info',headers=self.headers)   #client.get 是请求路径
        # print(r.status_code)
        # assert r.status_code ==200



# 用于设置性能测试 的一些参数
class WebsiteUser(HttpUser):
    tasks = [Testlocust]  # 指向一个定义的用户行为类






    # 执行事务之间用户等待时间的下界和上限
    min_wait=1500
    max_wait=5000

if __name__ == '__main__':
    import os
    os.system('locust -f page_info2.py ')

