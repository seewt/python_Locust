from locust import HttpUser,TaskSet,task

class Test_posson(TaskSet):
    # 每个虚拟用户测试期间，会先执行一次该方法（在一次测试过程中（比如测试1分钟，
    # 期间这个用户再次发起请求，不会再次执行该方法），只会执行一次）
    def on_start(self):
        print('登录')
    # 每个虚拟用户 执行前 会执行一次该方法（在一次测试结束后，只会执行一次）
    def on_stop(self):
        print('登出')

    @task()
    def task_1(self):
        print('任务1')
    @task()
    def task_2(self):
        print('任务2')

class setting(HttpUser):
    tasks = [Test_posson]
    # 执行事务之间用户等待时间的下界和上限
    min_wait = 2000
    max_wait = 5000


if __name__ == '__main__':
    import os
    os.system('locust -f locust_初识.py --host=http://www.baidu.com/')
