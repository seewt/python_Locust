# 一个事物下不同任务权重 @task(int：num)
from locust import TaskSet, HttpUser, task, Locust, SequentialTaskSet
import os
import jsonpath


''' 
继承TaskSet，不按顺序执行任务,
'''
class task_s(TaskSet):

    @task
    def task_1(self):

        get_url = 'http://wthrcdn.etouch.cn/weather_mini?city=杭州'
        r_request = self.client.request(method='get', url=get_url)
        # 返回的是 requests.models.Response 实例对象
        # print('通过request请求:', r_request.json())
        city = jsonpath.jsonpath(r_request.json(), '$..city')
        if city !=['杭州']:
            r_request.failure('Failed!')
        else:
            r_request.success()

    # 通过tasks 定义测试任务 并设置对应任务执行权重
    # tasks = {task1:1,task2:2,task3:2}
    # 通过tasks 定义测试任务


class task_conf(HttpUser):
    tasks = [task_s]
    min_time = 2000
    max_time = 5000


if __name__ == '__main__':
    os.system('locust -f 实战.py --web-host="127.0.0.1"')