

from locust import TaskSet, HttpUser, task, Locust, SequentialTaskSet
import os


''' 
继承SequentialTaskSet，按顺序执行任务,在使用装饰器 的情况下，
声明的顺序决定了执行的顺序。
'''
class task_s(SequentialTaskSet):

    @task
    def task1(self):
        print('执行任务1')
    @task
    def task2(self):
        print('执行任务2')
    @task
    def task3(self):
        print('执行任务3')




class task_conf(HttpUser):
    tasks = [task_s]
    min_time = 2000
    max_time = 5000


if __name__ == '__main__':
    os.system('locust -f task_sequence_by_tasks.py')