# -*- coding: utf-8 -*-
"""
 Date: 2023/2/14
 Author: byy
"""
import locust
from locust import task, FastHttpUser
import os

msgs = (i for i in range(100000000))


# productIds = [2565, 2566, 2567, 2568, 2569]
# p = (j for j in productIds)


class P1(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.25:8020'
    weight = 1
    msg_list = []

    def on_start(self):

        self.msg_num = 0
        self.data = {
            "dataid": "jc001",
            "uid": "001",
            "content": "",
            "actions": [],
            "robotId": "http old api",
            "seq": "9898556",
            "appid": "88888888888888888888888888888888888888",
            "productId": 2565,
            "projectId": 294
        }

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/inner/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P1.msg_list.append(self.msg_num)


class P2(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.25:8020'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "dataid": "jc002",
            "uid": "002",
            "content": "",
            "actions": [],
            "robotId": "http old api",
            "seq": "9898556",
            "appid": "88888888888888888888888888888888888888",
            "productId": 2566,
            "projectId": 294
        }

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/inner/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P2.msg_list.append(self.msg_num)


class P3(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.25:8020'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "dataid": "jc003",
            "uid": "003",
            "content": "",
            "actions": [],
            "robotId": "http old api",
            "seq": "9898556",
            "appid": "88888888888888888888888888888888888888",
            "productId": 2567,
            "projectId": 294
        }

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/inner/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P3.msg_list.append(self.msg_num)


class P4(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.25:8020'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "dataid": "jc004",
            "uid": "004",
            "content": "",
            "actions": [],
            "robotId": "http old api",
            "seq": "9898556",
            "appid": "88888888888888888888888888888888888888",
            "productId": 2568,
            "projectId": 294
        }

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/inner/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P4.msg_list.append(self.msg_num)


class P5(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.25:8020'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "dataid": "jc005",
            "uid": "005",
            "content": "",
            "actions": [],
            "robotId": "http old api",
            "seq": "9898556",
            "appid": "88888888888888888888888888888888888888",
            "productId": 2569,
            "projectId": 294
        }

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/inner/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P5.msg_list.append(self.msg_num)


# 全局测试停止后运行的函数
@locust.events.test_stop.add_listener
def on_test_stop(environment, **kwargs):
    print(f"产品1发送消息的总数{sum(P1.msg_list)}，剩余次数为{1000000 - sum(P1.msg_list)}\n"
          f"产品2发送消息的总数{sum(P2.msg_list)}，剩余次数为{1000000 - sum(P2.msg_list)}\n"
          f"产品3发送消息的总数{sum(P3.msg_list)}，剩余次数为{1000000 - sum(P3.msg_list)}\n"
          f"产品4发送消息的总数{sum(P4.msg_list)}，剩余次数为{1000000 - sum(P4.msg_list)}\n"
          f"产品5发送消息的总数{sum(P5.msg_list)}，剩余次数为{1000000 - sum(P5.msg_list)}\n")
    print(f"所有产品发送的消息总数为：{next(msgs)}")


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """

    os.system('locust -f inner_多产品.py '
              '-u 400 -r 40 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 300 '
              '--autostart '
              )
