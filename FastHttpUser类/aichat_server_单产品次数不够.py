# -*- coding: utf-8 -*-
"""
 Date: 2023/2/14
 Author: byy
"""
import locust
from locust import task, FastHttpUser
import os

msgs = (i for i in range(1000000))


# productIds = [2565, 2566, 2567, 2568, 2569]
# p = (j for j in productIds)


class P1(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):

        self.msg_num = 0
        self.data = {
            "async": True,
            "dataid": "mts002",
            "content": "",
            "actions": [],
            "seq": "0011",
            "userIdentity": "user11",
            "robotId": "robotId11",
            "appid": "appid11"
        }

    @task
    def chat(self):
        p1token = {
            "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTc3NzE2NDMyLCJwcm9kdWN0SWQiOjQxOCwiand0X3Rva2VuX2lkIjoiMTU1NmIzZjgtNGUwOC00NWM1LTkyMGItZjZlOGIyZDAyMjliIiwiand0X2V4cGlyZVRpbWUiOjE2NzY5ODQ5MTY0MzEsInByb2plY3RJZCI6NDUsImp3dF90b2tlbl90eXBlIjozfQ.GAu78ZjF1y4pdVBwVC3EJDeq918Xq8d3lXfpqLlNzsU"}
        self.msg_num += 1
        url = '/ai-ccs/v1/server/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data, headers=p1token) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 8301:
                    resp.success()
                else:
                    resp.failure(f'{resp.text}')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P1.msg_list.append(self.msg_num)


# 全局测试停止后运行的函数
@locust.events.test_stop.add_listener
def on_test_stop(environment, **kwargs):
    print(f"产品1发送消息的总数{sum(P1.msg_list)}，剩余次数为{1000000 - sum(P1.msg_list)}\n")
    print(f"所有产品发送的消息总数为：{next(msgs)}")


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """
    os.system('locust -f aichat_server_单产品次数不够.py '
              '-u 100 -r 50 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 300 '
              '--autostart '
              )
