# -*- coding: utf-8 -*-
"""
 Date: 2023/2/14
 Author: byy
"""
import locust
from locust import task, FastHttpUser
import os

msgs = (i for i in range(1000000))


# productIds = [2565, 2566, 2567, 2568, 2569]
# p = (j for j in productIds)


class P1(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.25:8020'
    weight = 1
    msg_list = []

    def on_start(self):

        self.msg_num = 0
        self.data = {
            "async": True,
            "dataid": "jc001",
            "content": "",
            "actions": [],
            "seq": "001",
            "userIdentity": "user1",
            "robotId": "robotId1",
            "appid": "appid1"
        }

    @task
    def chat(self):
        p1token =  {
            "X-Bm-ProjectId": "294",
            "X-Bm-ProductId": "2565"}
        self.msg_num += 1
        url = '/ai-ccs/v1/server/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data, headers=p1token) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure(f'{resp.text}')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P1.msg_list.append(self.msg_num)


# 全局测试停止后运行的函数
@locust.events.test_stop.add_listener
def on_test_stop(environment, **kwargs):
    print(f"产品1发送消息的总数{sum(P1.msg_list)}，剩余次数为{1000000 - sum(P1.msg_list)}\n")
    print(f"所有产品发送的消息总数为：{next(msgs)}")


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """
    os.system('locust -f aichat_server_不经过网关聊天-单产品.py '
              '-u 200 -r 20 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 300 '
              '--autostart '
              )
