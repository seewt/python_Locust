# -*- coding: utf-8 -*-
"""
 Date: 2023/2/14
 Author: byy
"""
import locust
from locust import task, FastHttpUser
import os
import uuid


class P1(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.21:8089'
    msg_list = []

    def on_start(self):
        self.msg_num = 0

    @task
    def consume(self):
        data = {
            "abilityId": "80001",
            "bmUser": "1625017240027926528",
            "consumeType": 1,
            "count": 1,
            "cusUser": "",
            "productId": 2565,
            "projectId": 294,
            "serverType": 1,
            "messageId": f"{uuid.uuid1()}"
        }
        url = '/intellect-saas/feign/productAbility/consume'
        with self.client.request(method='post', url=url, catch_response=True, json=data) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                    self.msg_num += 1
                else:
                    resp.failure(f'{resp.text}')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P1.msg_list.append(self.msg_num)


# 全局测试停止后运行的函数
@locust.events.test_stop.add_listener
def on_test_stop(environment, **kwargs):
    print(f"扣减的次数为{sum(P1.msg_list)}，剩余次数为{1000000 - sum(P1.msg_list)}\n")


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """
    os.system('locust -f intellect-saas_消费接口.py '
              '-u 100 -r 10 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 300 '
              '--autostart '
              )
