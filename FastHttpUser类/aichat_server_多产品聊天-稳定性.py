# -*- coding: utf-8 -*-
"""
 Date: 2023/2/14
 Author: byy
"""
import locust
from locust import task, FastHttpUser
import os

msgs = (i for i in range(10000000))



class P1(FastHttpUser):
    wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):

        self.msg_num = 0
        self.data = {
            "async": True,
            "dataid": "jc001",
            "content": "",
            "actions": [],
            "seq": "001",
            "userIdentity": "user1",
            "robotId": "robotId1",
            "appid": "appid1"
        }
        url = '/gateway/v1/saas/authorize'
        data = {
            "productKey": "09b50ffd96574d2ab0c9d756026faa2a",
            "productCode": "bmdlanhrcyj5tbx3ns"
        }
        resp = self.client.post(url=url, json=data).json()
        token = resp["data"]["accessToken"]
        self.headers = {"X-Bm-Access-Token": token}

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/server/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data,
                                 headers=self.headers) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P1.msg_list.append(self.msg_num)


class P2(FastHttpUser):
    wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "async": True,
            "dataid": "jc002",
            "content": "",
            "actions": [],
            "seq": "002",
            "userIdentity": "user2",
            "robotId": "robotId2",
            "appid": "appid2"
        }
        url = '/gateway/v1/saas/authorize'
        data = {
            "productKey": "83ee087d6bb642c3b278b78ef94be178",
            "productCode": "bmfxqkufqha4gp2uhl"
        }
        resp = self.client.post(url=url, json=data).json()
        token = resp["data"]["accessToken"]
        self.headers = {"X-Bm-Access-Token": token}

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/server/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data,
                                 headers=self.headers) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P2.msg_list.append(self.msg_num)


class P3(FastHttpUser):
    wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "async": True,
            "dataid": "jc003",
            "content": "",
            "actions": [],
            "seq": "003",
            "userIdentity": "user3",
            "robotId": "robotId3",
            "appid": "appid3"
        }
        url = '/gateway/v1/saas/authorize'
        data = {
            "productKey": "9ec75ed31dd446c18fa871713ca6d0aa",
            "productCode": "bmekvd0vprzz03x0p1"
        }
        resp = self.client.post(url=url, json=data).json()
        token = resp["data"]["accessToken"]
        self.headers = {"X-Bm-Access-Token": token}

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/server/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data,
                                 headers=self.headers) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P3.msg_list.append(self.msg_num)


class P4(FastHttpUser):
    wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "async": True,
            "dataid": "jc004",
            "content": "",
            "actions": [],
            "seq": "004",
            "userIdentity": "user4",
            "robotId": "robotId4",
            "appid": "appid4"
        }
        url = '/gateway/v1/saas/authorize'
        data = {
            "productKey": "9f895fa75413424c800565bc6fa36016",
            "productCode": "bmxwjaibtpmpfzbeyl"
        }
        resp = self.client.post(url=url, json=data).json()
        token = resp["data"]["accessToken"]
        self.headers = {"X-Bm-Access-Token": token}

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/server/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data,
                                 headers=self.headers) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P4.msg_list.append(self.msg_num)


class P5(FastHttpUser):
    wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "async": True,
            "dataid": "jc005",
            "content": "",
            "actions": [],
            "seq": "005",
            "userIdentity": "user5",
            "robotId": "robotId5",
            "appid": "appid5"
        }
        url = '/gateway/v1/saas/authorize'
        data = {
            "productKey": "08000d5fc33641e093e9a05996201313",
            "productCode": "bmuaczdmdfxnt6glls"
        }
        resp = self.client.post(url=url, json=data).json()
        token = resp["data"]["accessToken"]
        self.headers = {"X-Bm-Access-Token": token}

    @task
    def chat(self):
        self.msg_num += 1
        url = '/ai-ccs/v1/server/aichat'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data,
                                 headers=self.headers) as resp:
            # print(resp.text)
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P5.msg_list.append(self.msg_num)


# 全局测试停止后运行的函数
@locust.events.test_stop.add_listener
def on_test_stop(environment, **kwargs):
    print(f"产品1发送消息的总数{sum(P1.msg_list)}，剩余次数为{10000000 - sum(P1.msg_list)}\n"
          f"产品2发送消息的总数{sum(P2.msg_list)}，剩余次数为{10000000 - sum(P2.msg_list)}\n"
          f"产品3发送消息的总数{sum(P3.msg_list)}，剩余次数为{10000000 - sum(P3.msg_list)}\n"
          f"产品4发送消息的总数{sum(P4.msg_list)}，剩余次数为{10000000 - sum(P4.msg_list)}\n"
          f"产品5发送消息的总数{sum(P5.msg_list)}，剩余次数为{10000000 - sum(P5.msg_list)}\n")
    print(f"所有产品发送的消息总数为：{next(msgs)}")


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """
    os.system('locust -f aichat_server_多产品聊天-稳定性.py '
              '-u 100 -r 10 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 15h '
              '--autostart '
              )
