# -*- coding: utf-8 -*-
"""
 Date: 2023/2/14
 Author: byy
"""
import locust
from locust import task, FastHttpUser
import os

msgs = (i for i in range(1000000))


# productIds = [2565, 2566, 2567, 2568, 2569]
# p = (j for j in productIds)


class P1(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):

        self.msg_num = 0
        self.data = {
            "tokenType": "p"
        }

    @task
    def chat(self):
        p1token = {
            "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTEzLCJwcm9kdWN0SWQiOjI1NjUsImp3dF90b2tlbl9pZCI6Ijk5MWE4NWM5LTJjMzEtNDg0MS05YWViLTdlNDFlYTg3ZjA3ZSIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTEyLCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.fn5LCnKaW4kQ_UYJ4V5xm7VAcpSgoivEeq6cym0BYJc"}
        self.msg_num += 1
        url = '/mock-server-v2/api/ptoken'
        with self.client.request(method='post', url=url, catch_response=True, json=self.data, headers=p1token) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['data']
                if data == "p":
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P1.msg_list.append(self.msg_num)


class P2(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data =  {
            "tokenType": "p"
        }

    @task
    def chat(self):
        p2token = {
            "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTMyLCJwcm9kdWN0SWQiOjI1NjYsImp3dF90b2tlbl9pZCI6IjdkMTUxYTBkLTQwYjEtNGE4My1iNTJhLTAzNjQ4OTJlMGI1NCIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTMyLCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.DZYeKtGlQo28MZt0W-3-76Stmx-PQt73ytxgiKxZsCE"}

        self.msg_num += 1
        url = '/mock-server-v2/api/ptoken'
        with self.client.request(method='post', url=url, catch_response=True, json=self.data, headers=p2token) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['data']
                if data == "p":
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P2.msg_list.append(self.msg_num)


class P3(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "tokenType": "p"
        }

    @task
    def chat(self):
        p3token = {
            "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTQ4LCJwcm9kdWN0SWQiOjI1NjcsImp3dF90b2tlbl9pZCI6ImM4YjE4YjA2LWUwYzYtNDNhMy1iNmFkLThlMzA2ZTVkYmZmMCIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTQ4LCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.564iFhZNJkU02Qi8sfdLaYBZFGXhyE8kuFxyLNnnIYo"}

        self.msg_num += 1
        url = '/mock-server-v2/api/ptoken'
        with self.client.request(method='post', url=url, catch_response=True, json=self.data, headers=p3token) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['data']
                if data == "p":
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P3.msg_list.append(self.msg_num)


class P4(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data ={
            "tokenType": "p"
        }

    @task
    def chat(self):
        p4token = {
            "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTY0LCJwcm9kdWN0SWQiOjI1NjgsImp3dF90b2tlbl9pZCI6IjQ3OWM4MjlmLWE0ZTYtNDQzMC04ZDBiLWVmYzAzNTUwYjFiMCIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTY0LCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.mTV4owngON2bjh9SkTdoo2Sk8flgrQoFGXuoGc-Cwsk"}

        self.msg_num += 1
        url = '/mock-server-v2/api/ptoken'
        with self.client.request(method='post', url=url, catch_response=True, json=self.data, headers=p4token) as resp:
            # print(resp.text)
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['data']
                if data == "p":
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P4.msg_list.append(self.msg_num)


class P5(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):
        self.msg_num = 0
        self.data = {
            "tokenType": "p"
        }

    @task
    def chat(self):
        p5token = {
            "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTgwLCJwcm9kdWN0SWQiOjI1NjksImp3dF90b2tlbl9pZCI6ImEzMzEzYzNmLTA1MzItNDJkOC04ODIzLTRlYjIxMjg5ZDU4YyIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTgwLCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.xooFz-MnfrxIdhDwXK_VyoxmXqC5m3HBAQvpC-ZxGvA"}
        self.msg_num += 1
        url = '/mock-server-v2/api/ptoken'
        with self.client.request(method='post', url=url, catch_response=True, json=self.data, headers=p5token) as resp:
            # print(resp.text)
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['data']
                if data == "p":
                    resp.success()
                else:
                    resp.failure('请求失败')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        P5.msg_list.append(self.msg_num)


# 全局测试停止后运行的函数
# @locust.events.test_stop.add_listener
# def on_test_stop(environment, **kwargs):
#     print(f"产品1发送消息的总数{sum(P1.msg_list)}，剩余次数为{1000000 - sum(P1.msg_list)}\n"
#           f"产品2发送消息的总数{sum(P2.msg_list)}，剩余次数为{1000000 - sum(P2.msg_list)}\n"
#           f"产品3发送消息的总数{sum(P3.msg_list)}，剩余次数为{1000000 - sum(P3.msg_list)}\n"
#           f"产品4发送消息的总数{sum(P4.msg_list)}，剩余次数为{1000000 - sum(P4.msg_list)}\n"
#           f"产品5发送消息的总数{sum(P5.msg_list)}，剩余次数为{1000000 - sum(P5.msg_list)}\n")
#     print(f"所有产品发送的消息总数为：{next(msgs)}")


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """
    p1token = {
        "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTEzLCJwcm9kdWN0SWQiOjI1NjUsImp3dF90b2tlbl9pZCI6Ijk5MWE4NWM5LTJjMzEtNDg0MS05YWViLTdlNDFlYTg3ZjA3ZSIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTEyLCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.fn5LCnKaW4kQ_UYJ4V5xm7VAcpSgoivEeq6cym0BYJc"}
    p2token = {
        "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTMyLCJwcm9kdWN0SWQiOjI1NjYsImp3dF90b2tlbl9pZCI6IjdkMTUxYTBkLTQwYjEtNGE4My1iNTJhLTAzNjQ4OTJlMGI1NCIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTMyLCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.DZYeKtGlQo28MZt0W-3-76Stmx-PQt73ytxgiKxZsCE"}
    p3token = {
        "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTQ4LCJwcm9kdWN0SWQiOjI1NjcsImp3dF90b2tlbl9pZCI6ImM4YjE4YjA2LWUwYzYtNDNhMy1iNmFkLThlMzA2ZTVkYmZmMCIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTQ4LCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.564iFhZNJkU02Qi8sfdLaYBZFGXhyE8kuFxyLNnnIYo"}
    p4token = {
        "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTY0LCJwcm9kdWN0SWQiOjI1NjgsImp3dF90b2tlbl9pZCI6IjQ3OWM4MjlmLWE0ZTYtNDQzMC04ZDBiLWVmYzAzNTUwYjFiMCIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTY0LCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.mTV4owngON2bjh9SkTdoo2Sk8flgrQoFGXuoGc-Cwsk"}
    p5token = {
        "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2OTU3NjM1MTgwLCJwcm9kdWN0SWQiOjI1NjksImp3dF90b2tlbl9pZCI6ImEzMzEzYzNmLTA1MzItNDJkOC04ODIzLTRlYjIxMjg5ZDU4YyIsImp3dF9leHBpcmVUaW1lIjoxNjc2OTY0ODM1MTgwLCJwcm9qZWN0SWQiOjI5NCwiand0X3Rva2VuX3R5cGUiOjN9.xooFz-MnfrxIdhDwXK_VyoxmXqC5m3HBAQvpC-ZxGvA"}
    os.system('locust -f mock接口多产品.py '
              '-u 100 -r 50 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 300 '
              '--autostart '
              )
