from locust import task, HttpUser
import os


class client(HttpUser):
    host = 'http://172.16.0.48:18800'

    @task
    def task_1(self):
        headers = {
            "X-Bm-Token": "eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjg5Mjk5MTkxODg3MTgxLCJqd3RfbG9naW5UaW1lIjoxNjc2MzQxODEyNTgyLCJwcm9kdWN0SWQiOjQxOCwiand0X3Rva2VuX2lkIjoiNTc3NWRhYTctMjUyZS00Y2U3LWFhMTMtNTI4MDIxYTBiOGE5IiwiYXBwaWQiOiIyMTEiLCJrZXl0cCI6IjI1Iiwiand0X2V4cGlyZVRpbWUiOjE2NzYzNDkwMTI1ODIsInByb2plY3RJZCI6NDUsInNpZCI6MjQxLCJqd3RfdG9rZW5fdHlwZSI6MX0.MvQwAdLr1mNUJEEILOXP_htWDjdoieEPvSOBKsc-mLk"}
        url = '/mock-server-v2/api/opentoken'
        body = {
            "tokenType": "o"
        }

        try:
            self.client.post(url=url, json=body).json()
        except Exception as e:
            print(e)
            raise e



if __name__ == '__main__':
    """
    -u  用户数
    -r  每秒增加的用户数 
    """
    os.system('locust -f httpclient.py '
              '-u 50 -r 50 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 300 '
              '--autostart '
              )
