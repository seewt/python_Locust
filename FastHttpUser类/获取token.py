# -*- coding: utf-8 -*-
"""
 Date: 2023/2/21 
 Author: byy
"""

import requests

url = " http://172.16.0.48:18800/gateway/v1/saas/authorize"

keys = [
    {
        "productKey": "09b50ffd96574d2ab0c9d756026faa2a",
        "productCode": "bmdlanhrcyj5tbx3ns"
    },
    {
        "productKey": "83ee087d6bb642c3b278b78ef94be178",
        "productCode": "bmfxqkufqha4gp2uhl"
    },
    {
        "productKey": "9ec75ed31dd446c18fa871713ca6d0aa",
        "productCode": "bmekvd0vprzz03x0p1"
    },
    {
        "productKey": "9f895fa75413424c800565bc6fa36016",
        "productCode": "bmxwjaibtpmpfzbeyl"
    },
    {
        "productKey": "08000d5fc33641e093e9a05996201313",
        "productCode": "bmuaczdmdfxnt6glls"
    }

]

for key in keys:
    res = requests.post(url=url,json=key).json()
    print(res["data"]["accessToken"])
    print("--------------------")