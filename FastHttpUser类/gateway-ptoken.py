# -*- coding: utf-8 -*-
"""
 Date: 2023/2/27 
 Author: byy
"""
import locust
from locust import task, FastHttpUser
import os

msgs = (i for i in range(10000000))


class P1(FastHttpUser):
    wait_time = locust.constant(1)
    host = 'http://172.16.0.21:18800'
    weight = 1
    msg_list = []

    def on_start(self):

        self.msg_num = 0
        self.data = {
            "tokenType": "p"
        }

        url = '/gateway/v1/saas/authorize'
        data = {
            "productKey": "09b50ffd96574d2ab0c9d756026faa2a",
            "productCode": "bmdlanhrcyj5tbx3ns"
        }
        resp = self.client.post(url=url, json=data).json()
        token = resp["data"]["accessToken"]
        self.headers = {"X-Bm-Access-Token": token}

    @task
    def chat(self):
        self.msg_num += 1
        url = '/mock-server-v2/api/ptoken'
        self.data["content"] = f"{next(msgs)}"
        with self.client.request(method='post', url=url, catch_response=True, json=self.data,
                                 headers=self.headers) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure(f'{resp.text}')
            except Exception as e:
                resp.failure(e)

    def on_stop(self):
        pass
        # P1.msg_list.append(self.msg_num)


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """
    os.system('locust -f gateway-ptoken.py '
              '-u 100 -r 10 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 300 '
              '--autostart '
              )
