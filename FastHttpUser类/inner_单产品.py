# -*- coding: utf-8 -*-
"""
 Date: 2023/2/14
 Author: byy
"""
import locust
from locust import task, FastHttpUser
import os

g = (i for i in range(1000000))


class FastClient(FastHttpUser):
    # wait_time = locust.constant(1)
    host = 'http://172.16.0.25:8020'

    @task
    def test_token(self):
        url = '/ai-ccs/v1/inner/aichat'
        data = {
            "dataid": "jc001",
            "uid": "001",
            "content": f"{next(g)}",
            "actions": [],
            "robotId": "http old api",
            "seq": "9898556",
            "appid": "88888888888888888888888888888888888888",
            "productId": 2565,
            "projectId": 294
        }
        with self.client.request(method='post', url=url, catch_response=True, json=data) as resp:
            try:

                # 获取响应内容的status 字段值
                data = resp.json()['code']
                if data == 0:
                    resp.success()
                else:
                    resp.failure(f'{resp.text}')
            except Exception as e:
                resp.failure(e)


# 全局测试停止后运行的函数
@locust.events.test_stop.add_listener
def on_test_stop(environment, **kwargs):
    print(next(g))


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """
    os.system('locust -f inner_单产品.py '
              '-u 400 -r 40 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 120 '
              '--autostart '
              )
