from locust import task, FastHttpUser
import os
import logging as log


class task_s(FastHttpUser):
    host = 'http://wthrcdn.etouch.cn'

    @task
    def task_1(self):
        url = '/weather_mini?city=杭州'
        with self.client.request(method='get', url=url, catch_response=True) as resp:
            print(resp)



if __name__ == '__main__':
    os.system('start locust -f 通过FastHttpUser多进程发送请求.py  --master --web-host="127.0.0.1"')
    for i in range(5):
        os.system('start locust -f 通过FastHttpUser多进程发送请求.py  --worker')
