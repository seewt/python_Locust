# -*- coding: utf-8 -*-
"""
 Date: 2023/2/14
 Author: byy
"""
from locust import task, FastHttpUser
import os


class FastClient(FastHttpUser):
    host = 'http://172.16.2.105:8889'

    @task
    def test_token(self):
        url = '/mts/v1/aiChatReq'
        data = {
            "dataid": "mts002",
            "uid": "001",
            "content": "1",
            "actions": [],
            "robotId": "http old api",
            "seq": "9898556",
            "appid": "88888888888888888888888888888888888888",
            "productId": 418,
            "projectId": 45
        }
        with self.client.request(method='post', url=url, catch_response=True, json=data) as resp:
            try:
                # 获取响应内容的status 字段值
                code = resp.json()['code']
                if code == 0:
                    resp.success()
                else:
                    resp.failure('非法的参数')
            except Exception as e:
                resp.failure(e)


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """
    os.system('locust -f mts_test.py '
              '-u 100 -r 50 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 3600 '
              '--autostart '
              )
