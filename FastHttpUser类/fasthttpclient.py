# -*- coding: utf-8 -*-
"""
 Date: 2023/2/14
 Author: byy
"""
from locust import task, FastHttpUser
import os


class FastClient(FastHttpUser):
    host = 'http://172.16.0.21:18800'

    @task
    def test_token(self):
        url = '/mock-server-v2/api/opentoken'
        headers = {
            "X-Bm-Access-Token": "eyJhbGciOiJIUzI1NiJ9.eyJqd3RfbG9naW5UaW1lIjoxNjc2MzYzNzg3ODUyLCJwcm9kdWN0SWQiOjU2Miwiand0X3Rva2VuX2lkIjoiMTQ3MjY5ZTYtYzRmNy00NGM3LWIyZTgtMTg1ZTliMmJjZDQ5Iiwiand0X2V4cGlyZVRpbWUiOjE2NzYzNzA5ODc4NTIsInByb2plY3RJZCI6MjkyLCJqd3RfdG9rZW5fdHlwZSI6M30.1S_wmY2oO9xKv5C9srTs2lp3niqQF9ao0Qv1uRZLN5c"}
        data = {
            "tokenType": "o"
        }
        with self.client.request(method='post', url=url, catch_response=True, json=data) as resp:
            try:
                # 获取响应内容的status 字段值
                data = resp.json()['data']
                if data == "o":
                    resp.success()
                else:
                    resp.failure('非法的参数')
            except Exception as e:
                resp.failure(e)


if __name__ == '__main__':
    """
       -u  用户数
       -r  每秒增加的用户数 
       """
    os.system('locust -f fasthttpclient.py '
              '-u 10 -r 10 '
              # '--headless '
              '--web-host \"127.0.0.1\" '
              '--run-time 3600 '
              '--autostart '
              )
