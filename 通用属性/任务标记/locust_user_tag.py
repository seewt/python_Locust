from locust import User,tag
'''
设置tag，用于设置用户 执行指定标记的任务
'''

class myUser(User):

    @tag('回归')
    def task_1(self):
        print('回归任务')
    @tag('预发')
    def task_2(self):
        print('任务2')

    @tag('回归','预发')
    def task_3(self):
        print('回归任务+预发任务')

    tasks = [task_1,task_2,task_3]



if __name__ == '__main__':
    import os
    os.system('locust -f locust_user_tag.py --tags 回归 --web-host="127.0.0.1"')
