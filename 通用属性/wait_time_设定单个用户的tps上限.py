import time

from locust import constant, task, User, between, HttpUser,constant_throughput
import os

''' 
wait_time = constant() 等待一个固定的时间
'''


class Test_Wait(HttpUser):
    host = "http://172.16.0.48:18800"

    @task
    def task1(self):
        body = {
            "tokenType": "test"
        }

        res = self.client.post(url="/mock-server-v2/api/test", json=body).json()
        print(f"任务1{res}")

    @task
    def task2(self):
        body = {
            "tokenType": "test"
        }

        res = self.client.post(url="/mock-server-v2/api/test", json=body).json()
        print(f"任务2{res}")

    # 设定一个用户每s最多请求的次数，以此来设定tps（注意是单个用户的每秒的请求上限，不是所有用户，如果是多个用户，则tps累加）
    wait_time = constant_throughput(10)


if __name__ == '__main__':
    os.system('locust -f wait_time_设定单个用户的tps上限.py --headless -u 1 -r 1 --run-time 1m')
