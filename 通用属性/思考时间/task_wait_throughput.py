from locust import HttpUser,TaskSet,task,User,constant_throughput

'''
constant_throughput(10) 每秒最多运行10次任务

'''

class Test_posson(HttpUser):
    @task
    def req(self):
        get_url = 'http://wthrcdn.etouch.cn/weather_mini?city=杭州'
        res = self.client.request(method='get', url=get_url)
        print(res.json())
    wait_time = constant_throughput(10)

if __name__ == '__main__':
    import os
    os.system('locust -f task_wait_throughput.py --web-host="127.0.0.1"')
