from locust import constant, task,User
import os

''' 
wait_time = constant() 等待一个固定的时间
'''
class Task_1(User):

    @task
    def task1(self):
        print('task1----------')

    @task
    def task2(self):
        print('task2----------')

    @task
    def task3(self):
        print('task3----------')

    # 用户执行每个任务，会固定等待5s
    wait_time = constant(5)

if __name__ == '__main__':
    os.system('locust -f task_wait_constant.py --web-host "127.0.0.1"')