from locust import between, task, User
import os

''' 
wait_time = between() 等待一个最小值和最大值之间的随机时间 不含最大时间
'''

class Task_1(User):

    @task
    def task1(self):
        print('task1----------')

    @task
    def task2(self):
        print('task2----------')

    @task
    def task3(self):
        print('task3----------')

    # 用户执行每个任务，会固定等待[1,5) 秒
    wait_time = between(1,5)


if __name__ == '__main__':
    os.system('locust -f task_wait_between.py --web-host "127.0.0.1"')