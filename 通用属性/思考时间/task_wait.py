from locust import HttpUser,TaskSet,task,User,between,constant

class Test_posson(TaskSet):
    # 每个虚拟用户测试期间，会先执行一次该方法（在一次测试过程中（比如测试1分钟，
    # 期间这个用户再次发起请求，不会再次执行该方法），只会执行一次）
    def on_start(self):
        print('登录')
    # 每个虚拟用户 执行前 会执行一次该方法（在一次测试结束后，只会执行一次）
    def on_stop(self):
        print('登出')

    def task_1(self):
        print('任务1')

    def task_2(self):
        print('任务2')

    tasks = [task_1,task_2]

# 定义执行的用户的属性
class setting(User):
    # 指向要执行的任务
    tasks = [Test_posson]
    # 执行事务之间用户等待时间的下界和上限 ,等待1-5s
    # wait_time = between(1,5)
    wait_time = constant(5)


if __name__ == '__main__':
    import os
    os.system('locust -f task_wait.py ')
