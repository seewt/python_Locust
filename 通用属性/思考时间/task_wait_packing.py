from locust import HttpUser,task,constant_throughput,constant_pacing,between

'''
wait_time = constant_pacing(10)  每10s最多运行一次任务
'''

class Test_posson(HttpUser):
    @task
    def req(self):
        get_url = 'http://wthrcdn.etouch.cn/weather_mini?city=杭州'
        res = self.client.request(method='get', url=get_url)
        print(res.json())

    wait_time = constant_pacing(10)

if __name__ == '__main__':
    import os
    os.system('locust -f task_wait_throughput.py --web-host="127.0.0.1"')
