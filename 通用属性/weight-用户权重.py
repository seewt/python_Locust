from locust import User, HttpUser


"""
 weight 用于设定不同用户数量的权重，
 以下例子 app用户的数量 将比 web端的数量 的 3倍 

"""


class appUser(HttpUser):
    host = "http://172.16.0.48:18800"
    # weight = 3

    def task_1(self):
        body = {
            "tokenType": "test"
        }

        res = self.client.post(url="/mock-server-v2/api/test", json=body).json()
        # print(f"任务1{res}")

    tasks = [task_1]


class webUser(HttpUser):
    host = "http://172.16.0.48:18800"
    # weight = 1

    def task_A(self):
        body = {
            "tokenType": "test"
        }

        res = self.client.post(url="/mock-server-v2/api/test2", json=body).json()
        # print(f"任务A{res}")

    tasks = [task_A]


if __name__ == '__main__':
    import os
    os.system('locust -f weight-用户权重.py --headless -u 12 -r 12 --run-time 1m')
