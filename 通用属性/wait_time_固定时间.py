import time

from locust import constant, task, User
import os

''' 
wait_time = constant() 等待一个固定的时间
'''


class Test_Wait(User):

    @task
    def task1(self):
        print(f'任务1执行时间{time.strftime("%Y-%m-%d %H:%M:%S")}')

    @task
    def task2(self):
        print(f'任务2执行时间{time.strftime("%Y-%m-%d %H:%M:%S")}')

    @task
    def task3(self):
        print(f'任务3执行时间{time.strftime("%Y-%m-%d %H:%M:%S")}')

    # 用户执行每个任务，会固定等待5s
    wait_time = constant(5)


if __name__ == '__main__':
    os.system('locust -f wait_time_固定时间.py --web-host "127.0.0.1"')
