from locust import User
import os


# 通过tasks 定义测试任务
class task_1(User):

    def task1(self):
        print('执行任务1')

    def task2(self):
        print('执行任务2')

    def task3(self):
        print('执行任务3')


    tasks = [task1, task2, task3]
    # 通过tasks 定义测试任务 并设置对应任务执行权重
    # tasks = {task1:1,task2:2,task3:2}

if __name__ == '__main__':
    os.system('locust -f tasks属性声明用户任务.py --web-host="127.0.0.1"')