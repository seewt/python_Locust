# 一个事物下不同任务权重 @task(int：num)

from locust import task,User
import os

# 通过@task 定义用户任务 任务 ,同时还可以通过@task(int) 设置每个任务的权重
class task_2(User):

    @task(2)
    def task1(self):
        print('执行任务1')

    @task(2)
    def task2(self):
        print('执行任务2')

    @task(1)
    def task3(self):
        print('执行任务3')


if __name__ == '__main__':
    os.system('locust -f task装饰器声明任务.py --web-host="127.0.0.1"')