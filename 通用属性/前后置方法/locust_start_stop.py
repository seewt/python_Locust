import time

from locust import User, constant

'''
   每个虚拟用户 执行前
   会执行一次该方法（在一次测试结束后，只会执行一次）
'''


class myuser(User):
    wait_time = constant(2)

    def on_start(self):
        while 1:
            print(time.localtime())

    def on_stop(self):
        print('登出')

    def task_1(self):
        print('任务1')

    tasks = [task_1]


if __name__ == '__main__':
    import os

    os.system('locust -f locust_start_stop.py --web-host="127.0.0.1"')
