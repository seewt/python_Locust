# -*- coding: utf-8 -*-
"""
 Date: 2023/2/10 
 Author: byy
"""
import websocket
from websocket import create_connection


client = create_connection(url='ws://172.16.0.48:18800/ws-server/atoken?token=eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjE1NjE5Njk2NTQ3NDkxMzQ4NDgsImp3dF9sb2dpblRpbWUiOjE2NzY4NTc0NjAwNDAsInByb2R1Y3RJZCI6NDE4LCJqd3RfdG9rZW5faWQiOiI0OWJiNjkwYy0yMzdkLTRkNjEtYmNjOS1kOGMzM2NhOTY5ODQiLCJhcHBpZCI6ImJtYTgxMjI2NDE3NTYwNjYxMTk2OCIsImtleXRwIjoiY2FjYyIsImp3dF9leHBpcmVUaW1lIjoxNjc2ODY0NjYwMDQwLCJwcm9qZWN0SWQiOjQ1LCJzaWQiOiJpaWlpaWlpaWlpaSIsImp3dF90b2tlbl90eXBlIjoxfQ.OHlaFkNKfHDML5OUQ9GPasgexSbNhAZJMto7D5YecdE')
websocket.enableTrace(True)
while 1:
    msg = input("请输入发送的消息：")
    if msg.lower() !="close":
        client.send(msg)
        recv = client.recv()
        print(f"\n接收到消息:{recv}")
        continue
    client.close()
    break
