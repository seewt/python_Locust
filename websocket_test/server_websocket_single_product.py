# -*- coding: utf-8 -*-
"""
 Date: 2022/11/24 
 Author: byy
"""
import json
import re
import threading
import time
from concurrent.futures import ThreadPoolExecutor

import jsonpath
import requests
import websocket
from numpy import *
from loguru import logger

logger.remove()
t = time.strftime("%Y_%m_%d_%H%M%S")
logger.add(f"{t}.log", enqueue=False, rotation="100 MB")

data_report = dict()


class Ws:
    obj_num = 0

    def __init__(self, uri, heartbeat_interval, msg_interval, msg_totals):
        self.send_time = None
        self.connect_time = None
        self.auth_time = None
        self.uri = uri
        self.ws = None
        self.msg_running = None
        self.is_ai_chat = False
        self.msg_totals = msg_totals
        Ws.obj_num += 1
        self.socket_num = Ws.obj_num
        self.msg_interval = msg_interval
        self.heartbeat_interval = heartbeat_interval
        self.msg_num = 0  # 发送消息计数
        self.msg_recv = 0  # 收到消息的数量
        self.msg_timeout_num = 0  # 收到消息 ，但是接收超时的消息数
        self.times_total = 0
        self.timer = None
        # self.uid = uid

    def start(self):
        self.msg_running = True
        self.ws = websocket.WebSocketApp(self.uri,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close,
                                         on_open=self.on_open)
        self.ws.run_forever()

    def on_message(self, message):  # 服务器有数据更新时，主动推送过来的数据
        logger.info(f'连接{self.socket_num} 接收到的源消息{message}')
        # print(f'总消息接收{message}')
        # 等待验证消息
        if "OK" in message and "authResp" in message:
            self.auth_time = time.time()
            data_report[self.socket_num] = {}
            auth_elapsed_time = self.auth_time - self.connect_time
            heartbeat_pool.submit(self.heartbeat, f"socket-{self.socket_num}")
            aichat_pool.submit(self.ai_chat, self.socket_num, f'server-{self.socket_num}')
            data_report[self.socket_num].update({"connect_auth": auth_elapsed_time})

        if 'MSG' in message:
            self.msg_recv += 1
            data_report[self.socket_num].update({"msgs_recv": self.msg_recv})
            recv_time2 = time.time()
            # 取消息内容中的，该条发消息的时间
            msg_time = re.findall("\\${(.*?)}", message)[2]
            # 取消息内容中的，该条消息发的是第几条
            recv_num = re.findall("\\${(.*?)}", message)[1]
            # 取消息内容中的，该条消息是哪个连接发送的
            socket_num = re.findall("\\${(.*?)}", message)[0]

            logger.info(f'连接{self.socket_num} 收到{self.msg_recv}条消息，发送了第{self.msg_num}')
            logger.info(f"连接{self.socket_num} 接收消息《=={message}")
            # print(f'连接{self.socket_num} 收到{self.msg_recv}条消息，发送了第{self.msg_num}')
            # print(f"连接{self.socket_num} 接收消息《=={message}")

            # 获取响应时间
            elapsed_time = recv_time2 - float(msg_time)
            self.times_total += elapsed_time
            data_report[self.socket_num].update({"elapsed_time": self.times_total})
            if int(elapsed_time) > 1.0:
                logger.error(f'连接{socket_num}的第{recv_num}消息接收超时')
                self.msg_timeout_num += 1
                data_report[self.socket_num].update({"msgs_timeout": self.msg_timeout_num})
            logger.info(f"连接{socket_num}，第{recv_num}条消息的，响应时间为{elapsed_time}")

    def stop_msg(self):
        # print(f'连接{self.socket_num} 延时接收消息完成，关闭链接')
        logger.info(f'连接{self.socket_num} 延时接收消息完成，关闭链接')
        self.close_connect()

    def on_error(self, error):  # 程序报错时，就会触发on_error事件
        logger.error(f"连接{self.socket_num}连接异常，异常原因:{error}")
        # print(f"连接{self.socket_num}连接异常，异常原因:{error}")

    def on_close(self, code, reason):
        logger.warning(f"连接{self.socket_num}已关闭,状态码：{code},关闭原因{reason}")
        # print(f"连接{self.socket_num}已关闭,状态码：{code},关闭原因{reason}")

    def on_open(self):  # 连接到服务器之后就会触发on_open事件，这里用于send数据
        self.connect_time = time.time()
        # heartbeat_pool.submit(self.heartbeat, f"连接{self.socket_num}")
        # aichat_pool.submit(self.ai_chat, f"连接{self.socket_num}")
        logger.info(f"连接{self.socket_num}已建立，是否正常运行:{self.ws.keep_running}")

    # 业务层聊天
    def ai_chat(self, socket_name, uname):
        self.is_ai_chat = True
        while self.msg_num < self.msg_totals:
            self.msg_num += 1
            self.send_time = time.time()
            content = {
                "userIdentity": uname,
                "method": "aichatReq",
                "content": f"连接:${{{socket_name}}}=>MSG:${{{self.msg_num}}},time:${{{self.send_time}}}",
                "actions": [],
                "dataid": "mts002",
                "seq": f'{self.socket_num}_{int(self.send_time)}',
                "robotId": "server_robotId",
                "appid": "appid_test"

            }

            content_str = str(content)
            self.ws.send(content_str)

            data_report[self.socket_num].update({"msgs_sent": self.msg_num})
            logger.debug(f"连接{self.socket_num}发送消息==》{content_str}")
            # print(f"连接{self.socket_num}发送消息==》{content_str}")
            time.sleep(self.msg_interval)
        self.timer = threading.Timer(60, self.stop_msg)
        self.timer.start()

    # 业务层发送心跳
    def heartbeat(self, threadName):
        beat = {
            "method": "heartbeat",
            "code": 0,
            "msg": "ok",
            "name": threadName
        }
        while self.msg_running:
            self.ws.send(json.dumps(beat))
            logger.info(f"连接{self.socket_num}发送心跳消息{beat}")
            time.sleep(self.heartbeat_interval)

    def close_connect(self):
        self.ws.close()


# 数据统计，使用定时任务定时输出当前稳定性运行的测试数据指标，方便实时查看稳定性运行状态（备注：指标统计可能不是完全准确）
def data_statistics(data):
    global timer1
    msgs_sent = jsonpath.jsonpath(data, "$..msgs_sent")
    msgs_recv = jsonpath.jsonpath(data, "$..msgs_recv")
    connect_auth = jsonpath.jsonpath(data, "$..connect_auth")
    elapsed_time = jsonpath.jsonpath(data, "$..elapsed_time")
    msgs_timeout = jsonpath.jsonpath(data, "$..msgs_timeout")
    total_sent = sum(msgs_sent)
    total_recv = sum(msgs_recv)
    total_resp_time = sum(elapsed_time)
    total_timeout = sum(msgs_timeout)
    miss_num = total_sent - total_recv
    # print(data)
    logger.warning(
        f'\n+++++++++++++++++++++++++RESULT{time.strftime("%Y_%m_%d_%H:%M:%S")}++++++++++++++++++++++++++\n'
        f'发送的消息总数:{total_sent}\n'
        f'接收到消息的总数:{total_recv}\n'
        f'丢包数量:{miss_num}\n'
        f'丢包率:{miss_num / total_sent:.5%}\n'
        f'收发总耗时:{total_resp_time} s\n'
        f'平均响应时间:{total_resp_time / total_recv:.5} s\n'
        f'响应时间大于1s的消息数:{total_timeout}\n'
        f'响应时间大于1s的消息数占比：{total_timeout / total_recv:.5%}\n'
        f'认证平均响应时间{mean(connect_auth):.5} s'
        f'\n-----------------------------------------------------------------------------\n')

    print(f'\n+++++++++++++++++++++++++RESULT:{time.strftime("%Y_%m_%d_%H:%M:%S")}++++++++++++++++++++++++++\n'
          f'发送的消息总数:{total_sent}\n'
          f'接收到消息的总数:{total_recv}\n'
          f'丢包数量:{miss_num}\n'
          f'丢包率:{miss_num / total_sent:.5%}\n'
          f'收发总耗时:{total_resp_time} s\n'
          f'平均响应时间:{total_resp_time / total_recv:.5} s\n'
          f'响应时间大于1s的消息数:{total_timeout}\n'
          f'响应时间大于1s的消息数占比：{total_timeout / total_recv:.5%}\n'
          f'认证平均响应时间{mean(connect_auth):.5} s'
          f'\n-----------------------------------------------------------------------------\n')
    # 定时任务，每隔10s 统计一次数据 并输出到控制台和日志
    timer1 = threading.Timer(10, data_statistics, args=(data_report,))
    timer1.start()


def get_url(uids):
    for uid in range(uids):
        data = {
            "productKey": "193f67aa0dd1442abedc369f058637ba",
            "productCode": "bmgszba19nsxewphg0"
        }
        acc_service = "http://172.16.0.48:8050/intellect-auth/v1/saas/authorize"
        token = requests.post(url=acc_service, json=data).json()['data']['accessToken']
        wss_addr = f'ws://172.16.0.48/ai-ccs/v1/server/aichat?accessToken={token}'
        yield wss_addr


# url = "ws://82.157.123.54:9010/ajaxchattest"      # 开放的ws 测试地址


if __name__ == '__main__':
    timer1 = threading.Timer(10, data_statistics, args=(data_report,))
    timer1.start()
    aichat_pool = ThreadPoolExecutor(200, thread_name_prefix='aichat')  # 聊天线程池，控制同时聊天的个数
    heartbeat_pool = ThreadPoolExecutor(200, thread_name_prefix='heart')  # 心跳线程池，控制同时发送心跳的连接数，一般和连接数保持一致
    socket_pool = ThreadPoolExecutor(200, thread_name_prefix='socket')  # websocket连接数，即同时保持socket连接的个数
    # websocket.enableTrace(True)

    for url in get_url(200):
        wsclient = Ws(uri=url, heartbeat_interval=20, msg_interval=3, msg_totals=600)
        p2 = socket_pool.submit(wsclient.start)

    # 关闭线程池，并等待这个线程池执行完所有任务
    socket_pool.shutdown()
    # 取消定时任务
    timer1.cancel()
