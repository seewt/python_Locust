# -*- coding: utf-8 -*-
"""
 Date: 2022/11/24 
 Author: byy
"""
import json
import re
import time
from concurrent.futures import ThreadPoolExecutor
import requests
import websocket
from loguru import logger

from numpy import *

logger.remove()
t = time.strftime("%Y_%m_%d_%H%M%S")
logger.add(f"{t}.log", enqueue=False)

recv_msgs = []
total_msgs = []
times_total_msgs = []
diff_msgs = []
time_out_msgs = []
auth_total_times = []


class Ws:
    obj_num = 0

    def __init__(self, uri, heartbeat_interval, msg_interval, msg_totals, uid):
        self.send_time = None
        self.connect_time = None
        self.auth_time = None
        self.uri = uri
        self.ws = None
        self.msg_running = None
        self.is_ai_chat = False
        self.msg_totals = msg_totals
        Ws.obj_num += 1
        self.socket_num = Ws.obj_num
        self.msg_interval = msg_interval
        self.heartbeat_interval = heartbeat_interval
        self.msg_num = 0  # 发送消息计数
        self.msg_recv = 0  # 收到消息的数量
        self.msg_miss_num = 0  # 收发不一致的消息数
        self.msg_timeout_num = 0  # 收到消息 ，但是接收超时的消息数
        self.times_total = 0
        self.uid = uid

    def start(self):
        self.msg_running = True
        self.ws = websocket.WebSocketApp(self.uri,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close,
                                         on_open=self.on_open)
        self.ws.run_forever()

    def on_message(self, message):  # 服务器有数据更新时，主动推送过来的数据
        recv_time1 = time.time()
        # 等待验证消息
        if "OK" in message and "authResp" in message:
            self.auth_time = time.time()
            auth_elapsed_time = self.auth_time-self.connect_time
            auth_total_times.append(auth_elapsed_time)
            heartbeat_pool.submit(self.heartbeat, f"连接{self.socket_num}")
            aichat_pool.submit(self.ai_chat, f"连接{self.socket_num}")

        if 'MSG' in message:
            self.msg_recv += 1
            recv_time2 = time.time()
            msg_time = re.findall("\\${(.*?)}", message)[1]
            recv_num = re.findall("\\${(.*?)}", message)[0]
            logger.info(f'连接{self.socket_num} 收到{self.msg_recv}条消息，发送了第{self.msg_num}')
            logger.info(f"连接{self.socket_num} 接收消息《=={message}")
            if self.msg_recv < self.msg_totals:
                if self.msg_num == int(recv_num):
                    elapsed_time = recv_time2 - float(msg_time)
                    self.times_total += elapsed_time
                    logger.info(elapsed_time)
                    if int(elapsed_time) > 1.0:
                        logger.error(f'连接{self.socket_num}的第{recv_num}消息接收超时')
                        self.msg_timeout_num += 1

                elif self.msg_num != int(recv_num):
                    logger.error(f'连接{self.socket_num}==>收发不一致！发送第{self.msg_num}条的消息没有收到对应的响应 || 当前接收到的消息是第{recv_num}条')
                    self.msg_miss_num += 1

            elif self.msg_recv == self.msg_totals:
                logger.info(
                    f"结果统计: 连接=》 {self.socket_num} || "
                    f"发送消息数{self.msg_num} || 接收到消息数{self.msg_recv} || 丢失的消息数{self.msg_miss_num} || "
                    f"超时的消息数{self.msg_timeout_num} || 平均响应时间为 {self.times_total / self.msg_recv} || "
                    f"丢包率{(self.msg_num - self.msg_recv) / self.msg_num} ")

                self.msg_totals = 0
                self.msg_running = False
                self.close_connect()
                recv_msgs.append(self.msg_recv)
                total_msgs.append(self.msg_num)
                times_total_msgs.append(self.times_total)
                diff_msgs.append(self.msg_miss_num)
                time_out_msgs.append(self.msg_timeout_num)

            elif self.msg_recv > self.msg_totals:
                logger.error(f'连接{self.socket_num}==>通信异常，接收消息数{self.msg_recv}大于要发的总数{self.msg_totals}')
                logger.info(
                    f"结果统计: 连接=》 {self.socket_num} || "
                    f"发送消息数{self.msg_num} || 接收到消息数{self.msg_recv} || 丢失的消息数{self.msg_miss_num} || "
                    f"超时的消息数{self.msg_timeout_num} || 平均响应时间为 {self.times_total / self.msg_recv} || "
                    f"丢包率{(self.msg_num - self.msg_recv) / self.msg_num} ")
                self.msg_totals = 0
                self.msg_running = False
                self.close_connect()
                recv_msgs.append(self.msg_recv)
                total_msgs.append(self.msg_num)
                times_total_msgs.append(self.times_total)
                diff_msgs.append(self.msg_miss_num)
                time_out_msgs.append(self.msg_timeout_num)

        if self.is_ai_chat:
            if abs(recv_time1 - self.send_time) > float(self.heartbeat_interval + self.msg_interval):
                logger.error(f"连接{self.socket_num}==>websocket 通信异常，客户端主动关闭链接")
                logger.info(
                    f"结果统计: 连接=》 {self.socket_num} || "
                    f"发送消息数{self.msg_num} || 接收到消息数{self.msg_recv} || 丢失的消息数{self.msg_miss_num} || "
                    f"超时的消息数{self.msg_timeout_num} || 平均响应时间为 {self.times_total / self.msg_recv} || "
                    f"丢包率{(self.msg_num - self.msg_recv) / self.msg_num} ")
                self.msg_totals = 0
                self.msg_running = False
                self.close_connect()
                recv_msgs.append(self.msg_recv)
                total_msgs.append(self.msg_num)
                times_total_msgs.append(self.times_total)
                diff_msgs.append(self.msg_miss_num)
                time_out_msgs.append(self.msg_timeout_num)

    def on_error(self, error):  # 程序报错时，就会触发on_error事件
        logger.error(f"连接{self.socket_num}连接异常，异常原因:{error}")

    def on_close(self):
        logger.warning(f"连接{self.socket_num}已关闭")

    def on_open(self):  # 连接到服务器之后就会触发on_open事件，这里用于send数据
        self.connect_time = time.time()
        # heartbeat_pool.submit(self.heartbeat, f"连接{self.socket_num}")
        # aichat_pool.submit(self.ai_chat, f"连接{self.socket_num}")
        logger.info(f"连接{self.socket_num}已建立，是否正常运行:{self.ws.keep_running}")

    # 业务层聊天
    def ai_chat(self, threadName):
        self.is_ai_chat = True
        while self.msg_num < self.msg_totals:
            self.msg_num += 1
            self.send_time = time.time()
            content = {
                "method": "aichatReq",
                "content": f"{threadName}=>MSG:${{{self.msg_num}}},time:${{{self.send_time}}},uid:{self.uid}",
                "actions": [],
                "dataId": "mts002",
                "seq": f'{self.socket_num}_{int(self.send_time)}',
                "robotId": "test01",
                "appid": "appid_test"

            }
            content_str = str(content)
            self.ws.send(content_str)
            logger.debug(f"连接{self.socket_num}发送消息==》{content_str}")
            time.sleep(self.msg_interval)
        self.msg_running = False
        return

    # 业务层发送心跳
    def heartbeat(self, threadName):
        beat = {
            "method": "heartbeat",
            "code": 0,
            "msg": "ok",
            "name": threadName,
            "uid": self.uid
        }
        while self.msg_running:
            time.sleep(self.heartbeat_interval)
            self.ws.send(json.dumps(beat))

        return

    def close_connect(self):
        self.ws.close()


def get_url(uids):
    for uid in range(uids):
        data = {
            "uid": uid,
            "appid": "0",
            "productId": 418,
            "projectId": 45,
            "sid": "abcd",
            "keytp": "cacc"
        }
        acc_service = "http://172.16.0.48:8050/intellect-auth/feign/auth/genAtoken"
        token = requests.post(url=acc_service, json=data).json()['data']['accessToken']
        wss_addr = f'ws://172.16.0.48/ai-ccs/v1/client/aichat?token={token}'
        yield wss_addr, uid


# url = "ws://82.157.123.54:9010/ajaxchattest"      # 开放的ws 测试地址


if __name__ == '__main__':
    aichat_pool = ThreadPoolExecutor(20, thread_name_prefix='aichat')  # 聊天线程池，控制同时聊天的个数
    heartbeat_pool = ThreadPoolExecutor(200, thread_name_prefix='heart')  # 心跳线程池，控制同时发送心跳的连接数，一般和连接数保持一致
    socket_pool = ThreadPoolExecutor(200, thread_name_prefix='socket')  # websocket连接数，即同时保持socket连接的个数
    websocket.enableTrace(True)
    for url, uid in get_url(4800):
        wsclient = Ws(uri=url, heartbeat_interval=30, msg_interval=3, msg_totals=60, uid=uid)
        p2 = socket_pool.submit(wsclient.start)

    # 关闭线程池，并等待这个线程池执行完所有任务
    socket_pool.shutdown()
    sent_msgs = sum(total_msgs)
    get_msgs = sum(recv_msgs)
    loss_msgs = sent_msgs - get_msgs
    times_msgs = sum(times_total_msgs)
    timeout_msgs_total = sum(time_out_msgs)
    diff_msgs_total = sum(diff_msgs)
    logger.warning(
        f'\n##########################TEST-RESULT##############################\n'
        f'发送的消息总数:{sent_msgs}\n'
        f'接收到消息的总数:{get_msgs}\n'
        f'丢包数量:{loss_msgs}\n'
        f'丢包率:{loss_msgs / sent_msgs:.5%}\n'
        f'收发总耗时:{times_msgs} s\n'
        f'平均响应时间:{times_msgs / get_msgs:.5} s\n'
        f'响应时间大于1s的消息数:{timeout_msgs_total}\n'
        f'响应时间大于1s的消息数占比：{timeout_msgs_total / get_msgs:.5%}\n'
        f'收发不一致数量：{diff_msgs_total}\n'
        f'收发不一致数量占比：{diff_msgs_total / sent_msgs:.5%}\n'
        f'认证平均响应时间{mean(auth_total_times):.5} s'
        f'\n##############################END##################################\n')

    print(f'\n##########################TEST-RESULT##############################\n'
          f'发送的消息总数:{sent_msgs}\n'
          f'接收到消息的总数:{get_msgs}\n'
          f'丢包数量:{loss_msgs}\n'
          f'丢包率:{loss_msgs / sent_msgs:.5%}\n'
          f'收发总耗时:{times_msgs} s\n'
          f'平均响应时间:{times_msgs / get_msgs:.5} s\n'
          f'响应时间大于1s的消息数:{timeout_msgs_total}\n'
          f'响应时间大于1s的消息数占比：{timeout_msgs_total / get_msgs:.5%}\n'
          f'收发不一致数量：{diff_msgs_total}\n'
          f'收发不一致数量占比：{diff_msgs_total / sent_msgs:.5%}\n'
          f'认证平均响应时间{mean(auth_total_times):.5} s'
          f'\n##############################END##################################\n')
