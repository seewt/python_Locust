# -*- coding: utf-8 -*-
"""
 Date: 2023/2/10 
 Author: byy
 基于python locust 性能测试框架 websocket协议测试脚本 支持稳定性 & 性能测试
"""
import json
import logging
import os
import re

import requests
from locust import User, task, events, constant
import time
from websocket import create_connection
import logging.handlers


def logconf():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logName = time.strftime("%Y%m%d-%H%M%S")
    # 配置文件记录器，并设置最大文件大小为 10 MB
    file_handler = logging.handlers.RotatingFileHandler(f"{logName}.log", maxBytes=104857600, backupCount=1000,
                                                        encoding="utf-8")
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(module)s -%(lineno)s:  %(message)s")
    # 设置处理器输出的日志格式
    file_handler.setFormatter(formatter)
    # 将文件记录器添加到日志处理器中
    logger.addHandler(file_handler)


logconf()


# 测试执行成功时回调 回调后locust才会对其进行统计
def success_call(name, recvText, total_time, request_type="Success"):
    """

    :param name: 自定义请求名称
    :param recvText:  响应数据
    :param total_time: 响应时间
    :param request_type: 自定义请求类型
    :return:
    """
    events.request_success.fire(
        request_type=request_type,
        name=name,
        response_time=total_time,
        response_length=len(recvText)
    )


# 测试失败时回调 回调后locust才会对其进行统计
def fail_call(name, total_time, e):
    """

    :param name:  自定义请求名称
    :param total_time: 时间
    :param e: 异常对象
    :return:
    """
    events.request_failure.fire(
        request_type="Fail",
        name=name,
        response_time=total_time,
        response_length=0,
        exception=e,
    )


total_sent = []  # 每个协程发送消息总数
total_recv = []  # 每个协程接收消息总数


# 全局测试开始前运行的函数 ，如在所有虚拟用户测试前需要前置操作 可在此函数中增加逻辑
@events.test_start.add_listener
def on_test_start(environment, **kwargs):
    pass


# 全局测试停止后运行的函数
@events.test_stop.add_listener
def on_test_stop(environment, **kwargs):
    logging.info(f"总发送消息{sum(total_sent)}")
    logging.info(f"总接收消息{sum(total_recv)}")


class WebSocketClient(object):
    def __init__(self, host):
        self.host = host
        self.ws = None

    def connect(self, burl):
        self.ws = create_connection(burl)

    def recv(self):
        return self.ws.recv()

    def recv_frame(self):
        return self.ws.recv_frame()

    def send(self, msg):
        self.ws.send(msg)

    def close(self):
        self.ws.close()


class WebsocketUser(User):
    abstract = True

    def __init__(self, *args, **kwargs):
        super(WebsocketUser, self).__init__(*args, **kwargs)
        self.client = WebSocketClient(self.host)
        self.client._locust_environment = self.environment
        self.send_counts = 0
        self.recv_counts = 0
        self.uri = None


class WrongToken(WebsocketUser):
    host = ""
    # 每个请求之间的间隔
    wait_time = constant(1)

    # # 协程任务启动 前置方法
    def on_start(self):
        self.uri = f"ws://172.16.0.21:18800/ws-server/atoken?token=1111"

    @task
    def connection_close(self):
        self.client.connect(self.uri)
        msg = self.client.recv()
        mgs_dict = json.loads(msg)
        gateway_code = mgs_dict["code"]
        recv_frame = self.client.recv_frame()
        code = recv_frame.opcode
        data = recv_frame.data
        if gateway_code != 7003:
            success_call("没有正常断开", msg, 0.001, "Fail")
        if code != 8:
            success_call("ABNF Opcode 不为8", msg, 0.001, "Fail")

        success_call("收到token异常消息提醒", msg, 0.001)
        success_call("ABNF Opcode=8 websocket连接断开", data, 0.001)

    # def on_stop(self):
    #     logging.info(f'\n 测试结束 S{self.greenlet.name} \n发送消息总数 {self.send_counts} \n 接收消息总数 {self.recv_counts}')
    #     total_recv.append(self.recv_counts)
    #     total_sent.append(self.send_counts)
    #     self.client.close()


if __name__ == '__main__':
    """
    Usage: locust [OPTIONS] [UserClass ...]
    -f          运行指定的模块，未指定的情况下 locust将在当前目录下查找 locustfile.py 文件 进行运行
    --headless  不带 webui界面启动
    -u          模拟用户数量
    -r          每秒增加的用户数量
    --run-time  整体测试运行时间（注意 是所有用户执行完测试的时间，非单个虚拟用户的执行时间）
    -L          日志输出等级    DEBUG/INFO/WARNING/ERROR/CRITICAL
    --logfile   可选输出日志到 指定文件，未设置 则输入到控制台
    --web-host  设置 图形化界面 的访问地址，未设置 headless 的情况下，默认带webui界面启动，
    -s          在抵达测试结束时间后继续等待 一段时间，未设置的情况下 默认立即结束测试，
                （用于防止部分正在执行任务 而测试直接中止，注意：一定是有用户在执行任务中，这个设置才有意义）
    --autostart 自动运行
    --autoquit  运行结束后等待 x 秒后 自动停止
    -L          日志等级
    --logfile   日志路径
    """

    # 单进程运行，不启动图形化监控界面
    # os.system('locust -f wsClient_release.py --headless -u 1 -r 1  --run-time 3600s -L DEBUG')

    # 单进程运行，启动图形化监控界面
    # os.system('locust -f wsClient_release.py --web-host "127.0.0.1" --logfile=0504_15.log -L DEBUG  ')
    atoken = ""
    ptoken = ""
    stoken = ""
    wrong_token = ""
    os.system(f"locust "
              f"-f locust-websocket-错误的token.py "
              f"-u 500 -r 100 "
              f"--autostart "
              f"--web-host \"127.0.0.1\" "
              # f"--headless "
              f"--run-time 10m "
              # f"--logfile={logName}master.log "
              # f"-L DEBUG "
              f"--skip-log-setup "
              # f"--autoquit=20 "
              )

    # 多进程运行 提高并发
    # print(logName)
    # master_command = 'start locust -f wsClient_release.py  --master  --web-host="127.0.0.1" -L DEBUG --logfile=' + logName + 'master.log'
    # worker_command = 'start locust -f wsClient_release.py  --worker -L DEBUG  --logfile=' + logName + 'worker.log'
    # os.system(master_command)
    # for i in range(5):
    #     os.system(worker_command)
