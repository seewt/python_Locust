# -*- coding: utf-8 -*-
"""
 Date: 2022/11/24 
 Author: byy
"""
import asyncio
import websockets
import json
import random

token = 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjE1NjE5Njk2NTQ3NDkxMzQ4NDgsImp3dF9sb2dpblRpbWUiOjE2NjkyNTUyNTY1OTQsInByb2R1Y3RJZCI6NDE4LCJqd3RfdG9rZW5faWQiOiIyZjQ5NWJiZi04ZTZlLTQ0NjAtODZiMy01ZjMxYzMxYzAyODciLCJhcHBpZCI6IjAiLCJrZXl0cCI6ImNhY2MiLCJqd3RfZXhwaXJlVGltZSI6MTY2OTI2NjA1NjU5NCwicHJvamVjdElkIjo0NSwic2lkIjo1NTMzMzMzMzMsImp3dF90b2tlbl90eXBlIjoxfQ.kMc51USvnLgHZF9PWr6SMhr02_rCO8i3PijRjcrmXSk'

burl = f'ws://172.16.0.48:8020/ai-ccs/v1/client/aichat?token={token}'


async def mytest():
    websocket = await  websockets.connect(uri=burl)
    # num = random.randint(0, 10000000)
    for num in range(10):
        msg = {
            "method": "aichatReq",
            "content": num,
            "actions": [],
            "dataId": "tes006",
            "seq": num
        }
        msgstr = json.dumps(msg)
        await websocket.send(msgstr)
        print(f"↑: {msgstr}")
        while 1:
            greeting = await websocket.recv()
            print(f"↓: {greeting}")

asyncio.get_event_loop().run_until_complete(mytest())