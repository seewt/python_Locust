# -*- coding: utf-8 -*-
"""
 Date: 2022/4/28
 Author: byy
 v 3.0
"""
import logging
import os
import uuid
import datetime

from locust import User, task, events, constant_pacing
import time
from websocket import create_connection, WebSocketTimeoutException, _exceptions
import json
import random
import threading


# 自定义超时异常类
# class RespTimeOutException(Exception):
#     def __init__(self, total_time):
#         self.total_time = total_time
#
#     # 返回异常类对象的说明信息
#     def __str__(self):
#         return "请求响应超时，响应时间为{}".format(repr(self.total_time))

#
# class MsgMissException(Exception):
#     def __init__(self, sMsg_No, rMsg_No):
#         self.sMsg_No = sMsg_No
#         self.rMsg_No = rMsg_No
#
#     # 返回异常类对象的说明信息
#     def __str__(self):
#         return "发送的消息{}，接收的消息{}".format(self.sMsg_No, self.rMsg_No)


# 测试执行成功时回调 回调后locust才会对其进行统计
def success_call(name, recvText, total_time):
    events.request_success.fire(
        request_type="[Success]",
        name=name,
        response_time=total_time,
        response_length=len(recvText)
    )


# 测试失败时回调 回调后locust才会对其进行统计
def fail_call(name, total_time, e):
    events.request_failure.fire(
        request_type="[Fail]",
        name=name,
        response_time=total_time,
        response_length=0,
        exception=e,
    )


class WebSocketClient(object):
    def __init__(self, host):
        self.host = host
        self.ws = None

    def connect(self, burl):
        self.ws = create_connection(burl)

    def recv(self):
        return self.ws.recv()

    def send(self, msg):
        self.ws.send(msg)

    def close(self):
        self.ws.close()

    def shutdown(self):
        self.ws.shutdown()


class WebsocketUser(User):
    abstract = True

    def __init__(self, *args, **kwargs):
        super(WebsocketUser, self).__init__(*args, **kwargs)
        self.client = WebSocketClient(self.host)
        self.client._locust_environment = self.environment


class MySkUser(WebsocketUser):
    # constant_pacing(300) 每300s 最多执行一次sendMsg，这里的时间务必大于 --run-time
    wait_time = constant_pacing(2)
    host = ""
    isExit = False

    # 发送消息前初始化操作
    def on_start(self):
        self.client.connect("ws://127.0.0.1:8881/")
        self.client.send('hello')
        self.g = (i for i in range(100000000))

    # def eixtChat(self):
    #     self.isExit = True
    #     logging.info("[eixtChat]xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")

    @task(1)
    def sendMsg(self):
        self.client.send(f"{self.greenlet.name}=>msgnum:{next(self.g)}   {time.time()}")

    # 用户任务结束后操作,虚拟用户停止之后 依旧会执行 on_stop
    def on_stop(self):
        pass


if __name__ == '__main__':
    """
    Usage: locust [OPTIONS] [UserClass ...]
    -f          运行指定的模块，未指定的情况下 locust将在当前目录下查找 locustfile.py 文件 进行运行
    --headless  不带 webui界面启动
    -u          模拟用户数量
    -r          每秒增加的用户数量
    --run-time  整体测试运行时间（注意 是所有用户执行完测试的时间，非单个虚拟用户的执行时间）
    -L          日志输出等级    DEBUG/INFO/WARNING/ERROR/CRITICAL
    --logfile   可选输出日志到 指定文件，未设置 则输入到控制台
    --web-host  设置 webui    的访问地址，未设置 headless 的情况下，默认带webui界面启动，
    -s          在抵达测试结束时间后继续等待 一段时间，未设置的情况下 默认立即结束测试，
                （用于防止部分正在执行任务 而测试直接中止，注意：一定是有用户在执行任务中，这个设置才有意义）
    """

    # 单进程运行，不启动图形化监控界面
    os.system('locust -f locust_socket_client.py --headless -u 100 -r 100  --run-time 300s -L DEBUG')

    # 单进程运行，启动图形化监控界面
    # os.system('locust -f wsClient_release.py --web-host "127.0.0.1" --logfile=0504_15.log -L DEBUG  ')

    # 多进程运行&启动图形化监控界面
    # logName = time.strftime("%Y%m%d-%H%M%S")
    # print(logName)
    # master_command = 'start locust -f wsClient_release.py  --master  --web-host="127.0.0.1" -L DEBUG --logfile=' + logName + 'master.log'
    # worker_command = 'start locust -f wsClient_release.py  --worker -L DEBUG  --logfile=' + logName + 'worker.log'
    # os.system(master_command)
    # for i in range(5):
    #     os.system(worker_command)
