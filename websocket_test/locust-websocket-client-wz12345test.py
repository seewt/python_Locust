# -*- coding: utf-8 -*-
"""
 Date: 2023/2/10 
 Author: byy
"""
import logging
import os
import uuid
import datetime
from locust import User, task, events, constant_pacing
import time
from websocket import create_connection, WebSocketTimeoutException, _exceptions
import json
import random
import threading

g = (x for x in range(10000))

# appid 根据实际需要修改
appid = '4.0000r'

# ws 连接地址根据实际需要修改
ws_url = 'ws://172.16.3.27:1443/' + appid
# ws_url = 'ws://172.16.0.36:1443/' + appid
# 使用的是游客账号登录，uname 基本上是随机的，如有需要也可以更改
uname = 'guest'
token_Req = {
    "bmAppid": appid,
    "method": "loginReq",
    "statistics": {
        "维度二": "28",
        "channelNo": "小程序服务端渠道编号",
        "维度一": "渠道编号"
    },
    "uname": "ip_test8",
    "keytp": "guest"
}


# 自定义超时异常类
class RespTimeOutException(Exception):
    def __init__(self, total_time):
        self.total_time = total_time

    # 返回异常类对象的说明信息
    def __str__(self):
        return "请求响应超时，响应时间为{}".format(repr(self.total_time))


class MsgMissException(Exception):
    def __init__(self, sMsg_No, rMsg_No):
        self.sMsg_No = sMsg_No
        self.rMsg_No = rMsg_No

    # 返回异常类对象的说明信息
    def __str__(self):
        return "发送的消息{}，接收的消息{}".format(self.sMsg_No, self.rMsg_No)


# 获取登录的请求参数，构造随机
def get_token():
    token_Req['uname'] = uname + str(uuid.uuid4())
    # print('获取token时请求传参: %s' % token_Req)
    return token_Req


# 测试执行成功时回调 回调后locust才会对其进行统计
def success_call(name, recvText, total_time):
    events.request_success.fire(
        request_type="[Success]",
        name=name,
        response_time=total_time,
        response_length=len(recvText)
    )


# 测试失败时回调 回调后locust才会对其进行统计
def fail_call(name, total_time, e):
    events.request_failure.fire(
        request_type="[Fail]",
        name=name,
        response_time=total_time,
        response_length=0,
        exception=e,
    )


class WebSocketClient(object):
    def __init__(self, host):
        self.host = host
        self.ws = None

    def connect(self, burl):
        self.ws = create_connection(burl)

    def recv(self):
        return self.ws.recv()

    def send(self, msg):
        self.ws.send(msg)

    def close(self):
        self.ws.close()

    def shutdown(self):
        self.ws.shutdown()


class WebsocketUser(User):
    abstract = True

    def __init__(self, *args, **kwargs):
        super(WebsocketUser, self).__init__(*args, **kwargs)
        self.client = WebSocketClient(self.host)
        self.client._locust_environment = self.environment


class MySkUser(WebsocketUser):
    # constant_pacing(300) 每300s 最多执行一次sendMsg，这里的时间务必大于 --run-time
    wait_time = constant_pacing(3000000000000)
    host = ""
    isExit = False

    # 发送消息前初始化操作
    def on_start(self):
        # 登录参数
        login_Req = {
            "bmAppid": appid,
            "method": "loginReq",
            "statistics": {
                "维度二": "28",
                "channelNo": "小程序服务端渠道编号",
                "维度一": "渠道编号"
            },
            "uname": None
        }

        # 转人工参数
        kfConnReq = {
            "bmAppid": appid,
            "context": [
                {
                    "msg": [
                        "线程" + "转人工"
                    ],
                    "msgType": "common",
                    "type": "user"
                }
            ],
            "method": "kfConnReq",
            "uid": None
        }

        # 聊天参数
        kfchatReq = {
            "method": "kfchatReq",
            "uid": "1493821468067237888",
            "msgType": "common",
            "content": None,
            "chatPackSeq": None,
            "bmAppid": appid
        }

        # 退出人工参数
        quitKfReq = {
            "method": "quitKfReq",
            "uid": None,
            "chatPackSeq": None,
            "bmAppid": appid,
            "wxAppid": "wx16f2e50e562f4220"
        }

        # wss 地址
        self.url = ws_url
        self.g = (i for i in range(100000000))
        self.kfchatReq = kfchatReq
        self.quitKfReq = quitKfReq

        try:
            # 建立 websocket 连接
            self.client.connect(self.url)
            # 获取token
            token_param = get_token()
            token_start_time = time.time()
            logging.info('%s 用户请求token传参 %s' % (self.greenlet.name, token_param))
            self.client.send(json.dumps(token_param))
            kf_resp_token = self.client.recv()
            logging.info('%s 用户请求token响应 %s' % (self.greenlet.name, kf_resp_token))
            token = json.loads(kf_resp_token)['token']
            i = 1
            while token == '':
                print('%s 用户获取token失败,正在第%s尝试次重新获取' % (self.greenlet.name, str(i)))
                self.client.send(json.dumps(token_param))
                kf_resp_token = self.client.recv()
                token = json.loads(kf_resp_token)['token']
                i += 1
                if i >= 4:
                    logging.error('%s 用户超过3次获取token失败,接口响应 %s' % (self.greenlet.name, kf_resp_token))
                    # 虚拟用户无法获取到token，销毁用户
                    self.stop(force=True)
                    break
        except Exception as e:
            total_time = int((time.time() - token_start_time) * 1000)
            fail_call("login_req_token", total_time, e)  # 游客用户获取token过程统计
        else:
            total_time = int((time.time() - token_start_time) * 1000)
            success_call("login_req_token", "success", total_time)  # 游客用户获取token过程统计
            logging.info('%s用户 token=============%s ' % (self.greenlet.name, token))
            login_Req['uname'] = token

        try:
            # 拿到token登录 ,获取uid
            uid_start_time = time.time()
            self.client.send(json.dumps(login_Req))
            logging.info('%s用户 login------------%s ' % (self.greenlet.name, login_Req))
            self.uid = json.loads(self.client.recv())['uid']
            kfConnReq['uid'] = self.uid
            self.quitKfReq['uid'] = self.uid
        except Exception as e:
            total_time = int((time.time() - uid_start_time) * 1000)
            fail_call("login_req_uid", total_time, e)  # 游客用户获取token过程统计
        else:
            total_time = int((time.time() - uid_start_time) * 1000)
            success_call("login_req_uid", "success", total_time)  # 游客用户获取token过程统计

        try:
            # 拿到uid 转人工,获取 chatPackSeq
            kfConnReq_start_time = time.time()
            self.client.send(json.dumps(kfConnReq))
            kf_resp = self.client.recv()
            kf_resp_dict = json.loads(kf_resp)
            link_status = kf_resp_dict['status']
            logging.debug('用户%s发起转人工后返回消息%s' % (self.greenlet.name, kf_resp))

            # 判断转人工是否是队列中
            if link_status == 'link':
                self.chatPackSeqId = kf_resp_dict['chatPackSeq']
                kf_msg = self.client.recv()
                logging.info('%s 用户uid %s 直接进入会话中，接收到进场消息 %s' % (self.greenlet.name, self.uid, kf_msg))
                total_time = int((time.time() - kfConnReq_start_time) * 1000)
                success_call('直接进入转人工的用户', 'success', total_time)

            elif link_status == 'queue':
                queue_start_time = time.time()
                total_time = int((queue_start_time - kfConnReq_start_time) * 1000)
                success_call('进入过队列中的用户数', 'success', total_time)
                logging.info('%s 用户uid %s 处于队列中第%s位' % (self.greenlet.name, self.uid, kf_resp_dict['queueNum']))
                kf_resp_2 = self.client.recv()
                kf_resp_2_dict = json.loads(kf_resp_2)
                self.chatPackSeqId = kf_resp_2_dict['chatPackSeq']
                # 从队列中移除的用户数量，时间指标：进入队列到从队列中移除的 时间差
                total_time2 = int((time.time() - queue_start_time) * 1000)
                success_call('从队列中到会话中的用户数', 'success', total_time2)
                logging.info('用户 %s ,uid %s 从队列中进入会话中,接收到进场消息%s' % (self.greenlet.name, self.uid, kf_resp_2))

            else:
                logging.error('%s 用户uid %s 获取队列状态异常,获取到的队列消息为%s ' % (self.greenlet.name, self.uid, kf_resp))

            self.kfchatReq['chatPackSeq'] = self.chatPackSeqId
            self.quitKfReq['chatPackSeq'] = self.chatPackSeqId
            self.kfchatReq['uid'] = self.uid
            # self.client.ws.settimeout(300)

        except Exception as e:
            logging.error('%s 用户uid %s转人工异常，异常消息为 %s ' % (self.greenlet.name, self.uid, e))
            total_time = int((time.time() - kfConnReq_start_time) * 1000)
            fail_call("转人工失败的用户数", total_time, e)  # 游客用户登录过程的统计
        else:
            total_time = int((time.time() - kfConnReq_start_time) * 1000)
            success_call("转人工成功用户总数", "success", total_time)  # 游客用户登录过程的统计

    def eixtChat(self):
        self.isExit = True
        logging.info("[eixtChat]xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")

    @task(1)
    def sendMsg(self):
        # 用户发送聊天消息的数量
        logging.info("[sendMsg]xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
        timer = threading.Timer(500, self.eixtChat)
        timer.start()
        while True:
            if self.isExit:
                logging.info("[sendMsg] exit yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy")
                break
            # 用户发送聊天消息的间隔
            time.sleep(10)
            try:
                # print(msg_time)
                msg_Num = next(self.g)
                msg_content = 'user:%s_msgContent:%s_' % (self.uid, msg_Num)
                msg_time = time.time()
                self.kfchatReq['content'] = msg_content + str(msg_time)
                logging.info('用户%s || uid为%s =====》发送的的消息%s' % (self.greenlet.name, self.uid, self.kfchatReq))
                self.client.send(json.dumps(self.kfchatReq))
                recv_msg1 = self.client.recv()
                recv_msg1_dict = json.loads(recv_msg1)
                logging.info('用户%s收到服务端的响应消息111111%s' % (self.uid, recv_msg1))
                recv_msg2 = self.client.recv()
                recv_msg2_dict = json.loads(recv_msg2)
                logging.info('用户%s收到服务端的响应消息222222%s' % (self.uid, recv_msg2))

                if 'false' in recv_msg1 and msg_content in recv_msg1:
                    recv_msg_time = recv_msg1_dict['content'].split('_')[2]
                    total_time = (time.time() - float(recv_msg_time)) * 1000
                    if total_time > 2000:  # 时间差大于2000ms 则为超时
                        logging.warning(
                            '用户%s 接收到坐席的消息超时===== ，接收到的消息%s' % (self.uid, recv_msg1))
                        raise RespTimeOutException(total_time)

                elif 'false' in recv_msg2 and msg_content in recv_msg2:
                    recv_msg_time = recv_msg2_dict['content'].split('_')[2]
                    logging.info('用户%s 接收到了坐席主动发送的消息 %s' % (self.uid, recv_msg2))
                    # print('用户%s 收到坐席的消息%s 的时间%s'%(self.uid,recv_msg2,cur_time1))
                    total_time = (time.time() - float(recv_msg_time)) * 1000
                    if total_time > 2000:  # 时间差大于2000ms 则为超时
                        logging.warning(
                            '用户%s 接收到坐席的消息超时===== ，接收到的消息%s' % (self.uid, recv_msg2))
                        raise RespTimeOutException(total_time)
                else:
                    # 未收到坐席主动发送的消息
                    logging.info('用户%s 发送消息%s 后,坐席发送的消息存在丢失，或收发不一致' % (self.uid, msg_Num))
                    logging.info('用户%s 发送的消息%s<===>收到的消息%s' % (self.uid, msg_content, recv_msg2_dict['content']))
                    total_time = (time.time() - msg_time) * 1000
                    raise MsgMissException(msg_content, recv_msg2_dict['content'])

            except RespTimeOutException as e:  # 捕获超时异常。进行统计
                fail_call("Message_Timeout", total_time, e)

            except MsgMissException as e:
                fail_call("Message_Loss", total_time, e)

            # 捕获websocket 通信 超时异常
            except Exception as e:
                cur_time = time.time()
                fail_call("Message_Fail", (cur_time - msg_time) * 1000, e)
                logging.error('Message_Fail 的异常信息==========%s' % e)
                logging.error('用户%s发送消息后%s,长时间没有收到坐席消息，退出客服聊天，虚拟用户销毁' % (self.uid, self.kfchatReq))
                # self.client.send(json.dumps(self.quitKfReq))
                # self.client.shutdown()
                # self.stop(True)  # websocket通信超时300s后 虚拟用户销毁
            else:
                logging.info('用户%s 发送消息%s成功，' % (self.uid, self.kfchatReq))
                success_call("chat_req", "success", total_time)  # 统计成功的请求
        # 消息发送完成，退出客服聊天
        logging.info('用户%s 发送消息完成,发送退出客服聊天' % self.uid)
        self.client.send(json.dumps(self.quitKfReq))
        quit_msg = self.client.recv()
        logging.info('用户%s接收到退出聊天的消息%s' % (self.uid, quit_msg))
        print(quit_msg)
        logging.info('用户%s 关闭连接的' % self.uid)
        # self.client.close()
        self.client.shutdown()
        # print(self.client.recv())
        logging.info('用戶%s 断开socket连接 ' % self.uid)

    # 用户任务结束后操作,虚拟用户停止之后 依旧会执行 on_stop
    def on_stop(self):
        logging.info('%s 用户uid %s 发送消息任务结束' % (self.greenlet.name, self.uid))


if __name__ == '__main__':
    """
    Usage: locust [OPTIONS] [UserClass ...]
    -f          运行指定的模块，未指定的情况下 locust将在当前目录下查找 locustfile.py 文件 进行运行
    --headless  不带 webui界面启动
    -u          模拟用户数量
    -r          每秒增加的用户数量
    --run-time  整体测试运行时间（注意 是所有用户执行完测试的时间，非单个虚拟用户的执行时间）
    -L          日志输出等级    DEBUG/INFO/WARNING/ERROR/CRITICAL
    --logfile   可选输出日志到 指定文件，未设置 则输入到控制台
    --web-host  设置 webui    的访问地址，未设置 headless 的情况下，默认带webui界面启动，
    -s          在抵达测试结束时间后继续等待 一段时间，未设置的情况下 默认立即结束测试，
                （用于防止部分正在执行任务 而测试直接中止，注意：一定是有用户在执行任务中，这个设置才有意义）
    """

    # 单进程运行，不启动图形化监控界面
    # os.system('locust -f wsClient_release.py --headless -u 1 -r 1  --run-time 3600s -L DEBUG')

    # 单进程运行，启动图形化监控界面
    # os.system('locust -f wsClient_release.py --web-host "127.0.0.1" --logfile=0504_15.log -L DEBUG  ')

    # 多进程运行&启动图形化监控界面
    logName = time.strftime("%Y%m%d-%H%M%S")
    print(logName)
    master_command = 'start locust -f wsClient_release.py  --master  --web-host="127.0.0.1" -L DEBUG --logfile='+logName+'master.log'
    worker_command = 'start locust -f wsClient_release.py  --worker -L DEBUG  --logfile='+logName+'worker.log'
    os.system(master_command)
    for i in range(5):
        os.system(worker_command)
