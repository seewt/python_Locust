# -*- coding: utf-8 -*-
"""
 Date: 2022/12/17 
 Author: byy
"""
import requests


def get_url(uids):
    for uid in range(uids):
        data = {
            "productKey": "193f67aa0dd1442abedc369f058637ba",
            "productCode": "bmgszba19nsxewphg0"
        }
        acc_service = "http://172.16.0.48:8050/intellect-auth/v1/saas/authorize"
        token = requests.post(url=acc_service, json=data).json()['data']['accessToken']
        wss_addr = f'ws://172.16.0.48/ai-ccs/v1/server/aichat?accessToken={token}'
        yield wss_addr


def get_url2(nums):
    for num in range(nums):
        data = [{
            "productKey": "bf4d0bf1259e420db6da65f24a03cec8",
            "productCode": "bmfr25g3f5puca9k69",
            "dataid": "pro1"
        }, {
            "productKey": "ec60798435cc4f679c71ac6bd1c7ca0d",
            "productCode": "bms5t7ffhlzqsmoz8z",
            "dataid": "pro2"
        }, {
            "productKey": "37616258f8d84822aa1d1081f449283b",
            "productCode": "bmua6xyhv6ha1rslrb",
            "dataid": "pro3"
        }, {
            "productKey": "1a18ddbea7ad426da39914576588c0c6",
            "productCode": "bmfaarbifobmvjaf8g",
            "dataid": "pro4"
        }, {
            "productKey": "0f9e63c1e9c64ceb809f7191eea6631f",
            "productCode": "bmpkugmkqag0yb6zvd",
            "dataid": "pro5"
        }, {
            "productKey": "7a3af6f5b3be4f4ca40f32892e3c32b6",
            "productCode": "bmzfbxadmkqw2di9k4",
            "dataid": "pro6"
        }, {
            "productKey": "26f829318332485187b0c38bf7779acd",
            "productCode": "bmp3wcrzakf4hv53n3",
            "dataid": "pro7"
        }, {
            "productKey": "ea2940cf61b04ebdaa691882e6ef6dc2",
            "productCode": "bmk9jkqnzgr702gsqn",
            "dataid": "pro8"
        }, {
            "productKey": "fec8fa5184ce4e78a5ebe281b86dd9fa",
            "productCode": "bmlmhavbt6rtjkvj9x",
            "dataid": "pro9"
        }, {
            "productKey": "d4c694700f83472da85a64608993d88d",
            "productCode": "bm9yczzmcsrnflbj9f",
            "dataid": "pro10"
        }]
        for a in data:
            yield a,a["dataid"]


for url in get_url2(2):
    print(url)
