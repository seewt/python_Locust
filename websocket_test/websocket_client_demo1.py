# -*- coding: utf-8 -*-
"""
 Date: 2022/11/24 
 Author: byy
"""
import logging
import threading
import time
from concurrent.futures import ThreadPoolExecutor
import requests
from websocket import WebSocketApp
import websocket

pool = ThreadPoolExecutor(5)


class Ws:

    def __init__(self, uri):
        # self.num = 1
        self.uri = uri

    def start(self):
        self.ws = websocket.WebSocketApp(self.uri,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close,
                                         on_open=self.on_open)
        self.ws.run_forever()

    def on_message(self, message):  # 服务器有数据更新时，主动推送过来的数据
        print(f"{threading.current_thread().name} recive {message}")

    def on_error(self, error):  # 程序报错时，就会触发on_error事件
        print(error)

    @staticmethod
    def on_close(ws: WebSocketApp):
        print("Connection closed ……")

    def on_open(self):  # 连接到服务器之后就会触发on_open事件，这里用于send数据
        req = 'hello'
        self.ws.send(req)
        name = threading.current_thread().name
        pool.submit(self.send_msg, name)
        # t1 = threading.Thread(target=self.send_msg)
        # t1.start()

    def send_msg(self, name):
        # content = {
        #     "method": "aichatReq",
        #     "content": f"{threading.current_thread().name}=>{self.num}",
        #     "actions": [],
        #     "dataId": "tes006",
        #     "seq": 666666
        # }
        # content_str = str(content)
        num = 1
        while 1:
            time.sleep(1)
            self.ws.send(f"{name}=>msgnum={num}      {time.time()}")
            num += 1
            if num > 5:
                break

    def close(self):
        self.ws.close()


# url = "ws://82.157.123.54:9010/ajaxchattest"


if __name__ == '__main__':
    websocket.enableTrace(True)
    for i in range(200):
        # data = {
        #     "uid": None,
        #     "appid": "0",
        #     "productId": 418,
        #     "projectId": 45,
        #     "sid": 66666666666666,
        #     "keytp": "cacc"
        # }
        # url = "http://172.16.0.48:8050/intellect-auth/feign/auth/genAtoken"
        # data['uid'] = i
        # token = requests.post(url=url, json=data).json()['data']['accessToken']
        # burl = f'ws://172.16.0.48:8020/ai-ccs/v1/client/aichat?token={token}'
        burl = "ws://127.0.0.1:8881/"
        wsclient = Ws(burl)
        t = threading.Thread(target=wsclient.start, name=f'第{i}连接')
        t.start()
        # print(data)
