# -*- coding: utf-8 -*-
"""
 Date: 2022/11/24 
 Author: byy
"""
import threading
import time

from websocket import WebSocketApp
import websocket


def on_message(ws: WebSocketApp, message):  # 服务器有数据更新时，主动推送过来的数据
    print(message)


def on_error(ws: WebSocketApp, error):  # 程序报错时，就会触发on_error事件
    print(error)


def on_close(ws: WebSocketApp):
    print("Connection closed ……")


def send_msg(ws: WebSocketApp):
    num = 1
    while 1:
        time.sleep(1)
        content = {
            "method": "aichatReq",
            "content": num,
            "actions": [],
            "dataId": "tes006",
            "seq": 666666
        }
        num += 1
        content_str = str(content)

        ws.send(content_str)


def close(ws: WebSocketApp):
    ws.close()


def on_open(ws: WebSocketApp):  # 连接到服务器之后就会触发on_open事件，这里用于send数据
    # req = 'hello'
    # ws.send(req)
    t = threading.Thread(target=send_msg, args=(ws,))
    t.start()


if __name__ == '__main__':
    # websocket.enableTrace(True)
    token = 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjE1NjE2NTE0MDczMDI2MjMyMzIsImp3dF9sb2dpblRpbWUiOjE2NjkxNzAwODk4MjcsInByb2R1Y3RJZCI6NDE4LCJqd3RfdG9rZW5faWQiOiIyYjgxOWNmNC0zYmM3LTQxNjItYjJhYi0yMmE0YmI2ZGQwYjciLCJhcHBpZCI6IjEyM3JycnJycnJycjQiLCJrZXl0cCI6ImNhY2MiLCJqd3RfZXhwaXJlVGltZSI6MTY2OTE4MDg4OTgyNywicHJvamVjdElkIjo0NSwic2lkIjo2NjY2NjY2LCJqd3RfdG9rZW5fdHlwZSI6MX0.YuvSvR9-GkWR2gQ2lQG0FyfAg3aLZXE6SaLxWArHDDQ'
    url = "ws://82.157.123.54:9010/ajaxchattest"
    url1 = "ws://127.0.0.1:8888"
    burl = f'ws://172.16.0.48:8020/ai-ccs/v1/client/aichat?token={token}'
    # print(burl)
    ws = websocket.WebSocketApp(url=burl,
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close)
    ws.on_open = on_open
    ws.run_forever()
