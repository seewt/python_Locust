# -*- coding: utf-8 -*-
"""
 Date: 2022/11/24 
 Author: byy
"""
import json
import re
import threading
import time
from concurrent.futures import ThreadPoolExecutor

import jsonpath
import requests
import websocket
from loguru import logger

from numpy import *

logger.remove()
t = time.strftime("%Y_%m_%d_%H%M%S")
logger.add(f"{t}.log", enqueue=False)

data_report = dict()


class Ws:
    obj_num = 0

    def __init__(self, uri, heartbeat_interval, msg_interval, msg_totals, uid):
        self.send_time = None
        self.connect_time = None
        self.auth_time = None
        self.uri = uri
        self.ws = None
        self.msg_running = None
        self.is_ai_chat = False
        self.msg_totals = msg_totals
        Ws.obj_num += 1
        self.socket_num = Ws.obj_num
        self.msg_interval = msg_interval
        self.heartbeat_interval = heartbeat_interval
        self.msg_num = 0  # 发送消息计数
        self.msg_recv = 0  # 收到消息的数量
        self.mismatch = 0  # 收发不一致的消息数
        self.msg_timeout_num = 0  # 收到消息 ，但是接收超时的消息数
        self.times_total = 0
        self.uid = uid

    def start(self):
        self.msg_running = True
        self.ws = websocket.WebSocketApp(self.uri,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close,
                                         on_open=self.on_open)
        self.ws.run_forever()

    def on_message(self, message):  # 服务器有数据更新时，主动推送过来的数据
        recv_time1 = time.time()
        # 等待验证消息
        if "OK" in message and "authResp" in message:
            self.auth_time = time.time()
            data_report[self.socket_num] = {}
            auth_elapsed_time = self.auth_time - self.connect_time
            heartbeat_pool.submit(self.heartbeat, f"连接{self.socket_num}")
            aichat_pool.submit(self.ai_chat, f"连接{self.socket_num}")
            data_report[self.socket_num].update({"connect_auth": auth_elapsed_time})

        if 'MSG' in message:
            self.msg_recv += 1
            # 记录接收到的消息数
            data_report[self.socket_num].update({"msgs_recv": self.msg_recv})
            recv_time2 = time.time()

            # 获取消息体中,该消息发送时间
            msg_time = re.findall("\\${(.*?)}", message)[1]
            # 获取消息体中,该消息是第几条
            recv_num = re.findall("\\${(.*?)}", message)[0]

            elapsed_time = recv_time2 - float(msg_time)
            self.times_total += elapsed_time
            # 记录收发总耗时
            data_report[self.socket_num].update({"elapsed_time": self.times_total})
            logger.info(elapsed_time)
            logger.info(f'连接{self.socket_num} 收到{self.msg_recv}条消息，发送了第{self.msg_num}')
            logger.info(f"连接{self.socket_num} 接收消息《=={message}")
            if self.msg_recv < self.msg_totals:
                if self.msg_num == int(recv_num):
                    if int(elapsed_time) > 1.0:
                        logger.error(f'连接{self.socket_num}的第{recv_num}消息接收超时')
                        self.msg_timeout_num += 1
                        # 记录消息接收超时的个数
                        data_report[self.socket_num].update({"msg_recv_timeout_totals": self.msg_timeout_num})

                elif self.msg_num != int(recv_num):
                    logger.error(f'连接{self.socket_num}==>收发不一致！发送第{self.msg_num}条的消息没有收到对应的响应 || 当前接收到的消息是第{recv_num}条')
                    self.mismatch += 1
                    # 记录收发不一致
                    data_report[self.socket_num].update({"mismatch_msg_totals": self.mismatch})

            elif self.msg_recv == self.msg_totals:
                if self.msg_num != int(recv_num):
                    logger.error(f'连接{self.socket_num}==>收发不一致！发送第{self.msg_num}条的消息没有收到对应的响应 || 当前接收到的消息是第{recv_num}条')
                    self.mismatch += 1
                    # 记录收发不一致
                    data_report[self.socket_num].update({"mismatch_msg_totals": self.mismatch})

                if int(elapsed_time) > 1.0:
                    logger.error(f'连接{self.socket_num}的第{recv_num}消息接收超时')
                    self.msg_timeout_num += 1
                    # 记录消息接收超时的个数
                    data_report[self.socket_num].update({"msg_recv_timeout_totals": self.msg_timeout_num})

                self.msg_totals = 0
                self.msg_running = False
                self.close_connect()

            elif self.msg_recv > self.msg_totals:
                logger.error(f'连接{self.socket_num}==>通信异常，接收消息数{self.msg_recv}大于要发的总数{self.msg_totals}')
                self.msg_totals = 0
                self.msg_running = False
                self.close_connect()

        if self.is_ai_chat:
            if abs(recv_time1 - self.send_time) >= float(self.heartbeat_interval + self.msg_interval):
                logger.error(f"连接{self.socket_num}==>websocket 通信异常，客户端主动关闭链接")
                self.msg_totals = 0
                self.msg_running = False
                self.close_connect()

    def on_error(self, error):  # 程序报错时，就会触发on_error事件
        logger.error(f"连接{self.socket_num}连接异常，异常原因:{error}")

    def on_close(self):
        logger.warning(f"连接{self.socket_num}已关闭")

    def on_open(self):  # 连接到服务器之后就会触发on_open事件，这里用于send数据
        self.connect_time = time.time()
        logger.info(f"连接{self.socket_num}已建立，是否正常运行:{self.ws.keep_running}")

    # 业务层聊天
    def ai_chat(self, threadName):
        self.is_ai_chat = True
        while self.msg_num < self.msg_totals:
            self.msg_num += 1
            self.send_time = time.time()
            content = {
                "method": "aichatReq",
                "content": f"{threadName}=>MSG:${{{self.msg_num}}},time:${{{self.send_time}}},uid:{self.uid}",
                "actions": [],
                "dataId": "mts002",
                "seq": f'{self.socket_num}_{int(self.send_time)}',
                "robotId": "test01",
                "appid": "appid_test"

            }
            content_str = str(content)
            self.ws.send(content_str)
            data_report[self.socket_num].update({"msgs_sent_totals": self.msg_num})
            logger.debug(f"连接{self.socket_num}发送消息==》{content_str}")
            time.sleep(self.msg_interval)
        self.msg_running = False
        return

    # 业务层发送心跳
    def heartbeat(self, threadName):
        beat = {
            "method": "heartbeat",
            "code": 0,
            "msg": "ok",
            "name": threadName,
            "uid": self.uid
        }
        while self.msg_running:
            time.sleep(self.heartbeat_interval)
            self.ws.send(json.dumps(beat))

        return

    def close_connect(self):
        self.ws.close()


def data_statistics(data):
    global timer1
    msgs_sent = jsonpath.jsonpath(data, "$..msgs_sent_totals")  # 获取发送的消息数列表
    msgs_recv = jsonpath.jsonpath(data, "$..msgs_recv")  # 获取接收的消息数列表
    connect_auth = jsonpath.jsonpath(data, "$..connect_auth")  # 获取认证响应时间 列表
    elapsed_time = jsonpath.jsonpath(data, "$..elapsed_time")  # 获取所有消息响应时间列表
    msgs_timeout = jsonpath.jsonpath(data, "$..msg_recv_timeout_totals")  # 获取超时接收消息数列表
    msgs_mismatch = jsonpath.jsonpath(data, "$..mismatch_msg_totals")  # 获取收发不一致的消息数列表
    total_sent = sum(msgs_sent)  # 发送消息总数
    total_recv = sum(msgs_recv)  # 接收消息总数
    total_resp_time = sum(elapsed_time)  # 消息响应时间总和
    total_timeout = sum(msgs_timeout)  # 消息超时数总和
    total_mismatch = sum(msgs_mismatch)  # 收发不一致的消息数总和
    miss_num = total_sent - total_recv  # 丢失消息数
    # print(data)
    logger.warning(
        f'\n+++++++++++++++++++++++++RESULT{time.strftime("%Y_%m_%d_%H:%M:%S")}++++++++++++++++++++++++++\n'
        f'发送的消息总数:{total_sent}\n'
        f'接收到消息的总数:{total_recv}\n'
        f'丢包数量:{miss_num}\n'
        f'丢包率:{miss_num / total_sent:.5%}\n'
        f'收发不一致消息数量:{total_mismatch}\n'
        f'收发不一致消息数占比:{total_mismatch / total_recv :.5%}\n'
        f'收发总耗时:{total_resp_time} s\n'
        f'平均响应时间:{total_resp_time / total_recv:.5} s\n'
        f'响应时间大于1s的消息数:{total_timeout}\n'
        f'响应时间大于1s的消息数占比：{total_timeout / total_recv:.5%}\n'
        f'认证平均响应时间{mean(connect_auth):.5} s'
        f'\n-----------------------------------------------------------------------------\n')

    print(f'\n+++++++++++++++++++++++++RESULT{time.strftime("%Y_%m_%d_%H:%M:%S")}++++++++++++++++++++++++++\n'
          f'发送的消息总数:{total_sent}\n'
          f'接收到消息的总数:{total_recv}\n'
          f'丢包数量:{miss_num}\n'
          f'丢包率:{miss_num / total_sent:.5%}\n'
          f'收发不一致消息数量:{total_mismatch}\n'
          f'收发不一致消息数占比:{total_mismatch / total_recv :.5%}\n'
          f'收发总耗时:{total_resp_time} s\n'
          f'平均响应时间:{total_resp_time / total_recv:.5} s\n'
          f'响应时间大于1s的消息数:{total_timeout}\n'
          f'响应时间大于1s的消息数占比：{total_timeout / total_recv:.5%}\n'
          f'认证平均响应时间{mean(connect_auth):.5} s'
          f'\n-----------------------------------------------------------------------------\n')
    # 定时任务，每隔10s 统计一次数据 并输出到控制台和日志
    timer1 = threading.Timer(10, data_statistics, args=(data_report,))
    timer1.start()


def get_url(max_uid):
    for uid in range(max_uid):
        data = {
            "uid": uid,
            "appid": "0",
            "productId": 418,
            "projectId": 45,
            "sid": "abcd",
            "keytp": "cacc"
        }
        acc_service = "http://172.16.0.48:8050/intellect-auth/feign/auth/genAtoken"
        token = requests.post(url=acc_service, json=data).json()['data']['accessToken']
        wss_addr = f'ws://172.16.0.48/ai-ccs/v1/client/aichat?token={token}'
        yield wss_addr, uid


# url = "ws://82.157.123.54:9010/ajaxchattest"      # 开放的ws 测试地址


if __name__ == '__main__':
    timer1 = threading.Timer(10, data_statistics, args=(data_report,))
    timer1.start()
    aichat_pool = ThreadPoolExecutor(20, thread_name_prefix='aichat')  # 聊天线程池，控制同时聊天的个数
    heartbeat_pool = ThreadPoolExecutor(200, thread_name_prefix='heart')  # 心跳线程池，控制同时发送心跳的连接数，一般和连接数保持一致
    socket_pool = ThreadPoolExecutor(200, thread_name_prefix='socket')  # websocket连接数，即同时保持socket连接的个数
    websocket.enableTrace(True)
    for url, uid in get_url(4800):
        wsclient = Ws(uri=url, heartbeat_interval=30, msg_interval=3, msg_totals=60, uid=uid)
        p2 = socket_pool.submit(wsclient.start)
