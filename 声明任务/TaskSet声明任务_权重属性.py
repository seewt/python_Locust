
from locust import TaskSet, task,User,constant
import os

''' 
TaskSet类定义了每个用户的任务集合，
注意：如果存在多个任务集合，需要在每个任务集合中调用interrupt()方法，
否则用户一旦进入到这个用户，则无法跳出这个任务集合，转而去执行其他任务集
'''
class Task_1(TaskSet):

    @task
    def task1(self):
        print('task1----------')

    @task
    def task2(self):
        print('task2----------')

    @task
    def task3(self):
        print('task3----------')

    #调用interrupt() 非常重要，否则用户将难以自拔
    @task
    def logout(self):
        self.interrupt()

class Task_2(TaskSet):

    @task
    def task21(self):
        print('task21----------')

    @task
    def task22(self):
        print('task22----------')

    @task
    def task23(self):
        print('task23----------')

    @task
    def logout(self):
        self.interrupt()


class task_conf(User):
    wait_time = constant(0)
    tasks = {Task_2:2,Task_1:4}


if __name__ == '__main__':
    os.system('locust -f TaskSet声明任务_权重属性.py --web-host "127.0.0.1"')