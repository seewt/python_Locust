# 一个事物下不同任务权重 @task(int：num)

from locust import TaskSet, HttpUser, task, Locust, SequentialTaskSet,User
import os

''' 
继承TaskSet，不按顺序执行任务,
'''

# 通过tasks 定义测试任务
class task_1(TaskSet):

    def task1(self):
        print('执行任务1')

    def task2(self):
        print('执行任务2')

    def task3(self):
        print('执行任务3')

    # 通过tasks 定义测试任务 并设置对应任务执行权重
    # tasks = {task1:1,task2:2,task3:2}
    # 通过tasks 定义测试任务

    tasks = [task1, task2, task3]


# 通过@task 装饰器的方式定义测试任务
class task_2(TaskSet):

    @task
    def task1(self):
        print('执行任务2-----1')

    @task
    def task2(self):
        print('执行任务2-----2')

    @task
    def task3(self):
        print('执行任务2-----3')

    # 通过tasks 定义测试任务 并设置对应任务执行权重
    # tasks = {task1:1,task2:2,task3:2}
    # 通过tasks 定义测试任务


class task_conf(User):
    tasks = [task_1,task_2]


if __name__ == '__main__':
    os.system('locust -f Taskset类中定义任务的两种方式.py')