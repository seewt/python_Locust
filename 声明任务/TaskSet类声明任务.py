# 一个事物下不同任务权重 @task(int：num)

from locust import TaskSet, task,User,constant
import os

''' 
TaskSet类定义了每个用户的任务集合
'''
class Task_1(TaskSet):

    @task
    def task1(self):
        print('task1----------')

    @task
    def task2(self):
        print('task2----------')

    @task
    def task3(self):
        print('task3----------')

class task_conf(User):
    wait_time = constant(5)
    tasks = [Task_1]


if __name__ == '__main__':
    os.system('locust -f TaskSet类声明任务.py --web-host "127.0.0.1"')