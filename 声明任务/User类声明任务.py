from locust import TaskSet, task,User
import os

''' 
在User中定义任务
'''
class Task_1(User):

    @task
    def task1(self):
        print('task1----------')

    @task
    def task2(self):
        print('task2----------')

    @task
    def task3(self):
        print('task3----------')

if __name__ == '__main__':
    os.system('locust -f User类声明任务.py --web-host "127.0.0.1"')