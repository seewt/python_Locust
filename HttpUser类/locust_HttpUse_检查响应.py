from locust import HttpUser, task
import os
from json import JSONDecodeError

class task_s(HttpUser):
    host = 'http://wthrcdn.etouch.cn'

    @task
    def task_1(self):
        api = 'http://wthrcdn.etouch.cn/weather_mini?city=杭州'
        with self.client.request(method='get', url=api,catch_response=True) as response:
            try:
                # 获取响应内容的status 字段值
                status = response.json()['status']
                # 获取响应时间
                # rt = response.elapsed.total_seconds()
                # print(rt)

                # 如果status为1000则为成功，否则为失败
                if status == 1000:
                    response.success()
                    # if rt <= 0.001:
                    #     response.success()
                    # else:
                    #     response.failure('响应超时')
                else:
                    response.failure('非法的参数')
            except JSONDecodeError:
                response.failure('Response could not be decoded as JSON')

if __name__ == '__main__':
    os.system('locust -f locust_HttpUse_检查响应.py --web-host="127.0.0.1" ')