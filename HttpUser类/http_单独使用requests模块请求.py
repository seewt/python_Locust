from locust import HttpUser,task
import os,requests
'''
接口响应包含cookie 才能使用cookies 快速访问，
cookie 实际存在respons 的headers 里的Set-Cookie
Cookie 的返回对象为 RequestsCookieJar，它的行为和字典类似
'''

class TaskA(HttpUser):

    cookie = None

    def on_start(self):
        url = 'https://www.processon.com/login/quick_login'
        data = {'type': 'account_login',
                'login_email': 15557548962,
                'login_password': 'qa1234',
                }
        # r = self.client.request('post', url=url, data=data)
        r = requests.request('post',url=url, data=data)
        TaskA.cookie = r.cookies


    @task
    def test_login(self):
        url = 'https://www.processon.com/setting/account'
        # r = self.client.request('post', url=url)
        r = requests.request('post', url=url,cookies=TaskA.cookie)
        print(r.text)


if __name__ == '__main__':
    os.system('locust -f http_单独使用requests模块请求.py --web-host="127.0.0.1"')












