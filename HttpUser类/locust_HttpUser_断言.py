# 一个事物下不同任务权重 @task(int：num)

from locust import TaskSet, HttpUser, task, Locust, SequentialTaskSet
import os
import jsonpath


''' 
继承TaskSet，不按顺序执行任务,
'''
class task_s(HttpUser):
    host = 'http://wthrcdn.etouch.cn'
    @task
    def task_1(self):

        get_url = '/weather_mini?city=杭州'
        res = self.client.request(method='get', url=get_url)
        print(res.json())
        with self.client.request(method='get', url=get_url,catch_response=True) as response:
            city = jsonpath.jsonpath(response.json(), '$..city')
            if city !=['杭州']:
                response.failure('断言失败')
            #判断接口响应时间
            elif response.elapsed.total_seconds() >0.5:
                print(response.elapsed.total_seconds())
                response.failure('响应时间过长')
            else:
                response.success()



if __name__ == '__main__':
    os.system('locust -f locust_HttpUser_断言.py --web-host="127.0.0.1"')