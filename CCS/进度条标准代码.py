# -*- coding: utf-8 -*-
"""
 Date: 2023/4/4 
 Author: byy
"""
import time

import tqdm

def progress_bar(total, updates):
    """
    显示进度条，用于更新总数和每次更新增量值的大小
    """
    bar = tqdm.tqdm(total=total, unit='B')
    for i in range(int(total / updates)):
        remaining = total - i * updates
        progress = int(remaining * 100 / total)
        bar.update(progress)
        time.sleep(0.1)
    bar.close()

total = 1000
updates = 30
progress_bar(total, updates)


