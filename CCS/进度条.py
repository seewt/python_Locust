# -*- coding: utf-8 -*-
"""
 Date: 2023/4/4 
 Author: byy
"""
from tqdm import tqdm
import time
#total参数设置进度条的总长度
bar = tqdm(total=100) # total表示预期的迭代次数
for i in range(102): # 同上total值
    bar.update(1)  #每次更新进度条的长度
    time.sleep(0.01)