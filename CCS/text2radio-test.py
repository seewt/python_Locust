# -*- coding: utf-8 -*-
"""
 Date: 2023/2/28 
 Author: byy
"""
import requests
import jsonpath

import requests, time, threading

# 进度条代码

total_size = 0


def dlsplay_jd(size, totlasize):
    val = (size * 100) // totlasize
    print('\r当前进度%s%%|%s' % (val, '>' * (val // 3)), end='')


# decorator实现的是 将返回的视频内容写入到本地文件
def load2local(func):
    def wrapper(msgId, sentenceId):
        time_statr = time.time()
        res = func(msgId, sentenceId)
        dl_size = 0
        with open(f'{msgId}.mp3', 'wb') as f:
            for data in res.iter_content(chunk_size=1024):
                size = f.write(data)
                dl_size += size
                dlsplay_jd(dl_size, total_size)
        time_end = time.time()
        print(time_end - time_statr)

    return wrapper


@load2local
def dl(msgId: str, sentenceId: str):
    global total_size
    url = "http://172.16.0.21:8050/ai-ccs-v4/v2/voice/transferVoice"
    data = {"msgId": f"{msgId}", "sentenceId": sentenceId}
    res = requests.post(url=url, json=data)
    # print(res.text)
    total_size = int(res.headers['content-length'])
    return res


if __name__ == '__main__':

    while 1:
        msgId, sentenceId = input("请输入下载的参数:").split(",")
        # print(msgId,sentenceId)
        dl(msgId, sentenceId)
        # load2local(dl)(msgId)
