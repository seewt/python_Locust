# -*- coding: utf-8 -*-
"""
 Date: 2023/2/28 
 Author: byy
"""

import requests
import time
from tqdm import tqdm


# decorator实现的是 将返回的视频内容写入到本地文件
def load2local(func):
    def wrapper(msgId, sentenceId):
        time_statr = time.time()
        res, _total = func(msgId, sentenceId)
        bar = tqdm(total=_total, unit='B')
        with open(f'{msgId}.mp3', 'wb') as f:
            for data in res.iter_content(chunk_size=1024):
                f.write(data)
                bar.update(len(data))
            bar.close()
        time_end = time.time()
        print(time_end - time_statr)
    return wrapper


@load2local
def dl(msgId: str, sentenceId: str):
    global total_size
    url = "http://172.16.3.172:8090/ai-ccs/v1/voice/transferVoice"
    data = {"msgId": f"{msgId}", "sentenceId": sentenceId}
    res = requests.post(url=url, json=data)
    total_size = int(res.headers['content-length'])
    return res, total_size


if __name__ == '__main__':
    while 1:
        msgId, sentenceId = input("请输入下载的参数:").split(",")
        dl(msgId, sentenceId)
